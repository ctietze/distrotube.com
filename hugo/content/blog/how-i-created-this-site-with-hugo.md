---
title: "How I Created This Website Using Hugo"
date: 2020-01-01T12:22:40+06:00
image: images/blog/0001.jpg
author: Derek Taylor
---

### STEP-1 : Hugo installation

Check the [hugo install documentation](https://gohugo.io/getting-started/installing/) on how to install hugo on your computer.  You should install it on you local machine as this would be the place to work on drafts and tweak your template.  You may or may not want to install hugo to your remote machine as well.  This will depend on how you wish to deploy your site.


### STEP-2 : Create your project

Let's create a new project on our local machine.  Hugo provides a `new` command to create a new website.

```
hugo new site <new_project>
```

### STEP-3 : Install a theme

I started with a theme called navigator-hugo.  To install it, I had to run this command
```
hugo new site navigator-hugo
```
and then go to the themes folder inside of navigator-hugo folder. You can also use this command ```cd navigator-hugo/themes``` for going to this folder.
Then run the command 
```
git clone git@github.com:themefisher/navigator-hugo.git
```

Alternatively, you can [download the theme as .zip](https://github.com/themefisher/navigator-hugo/archive/master.zip) file and extract it in the `themes` directory

After that you need to go to the `navigator-hugo/exampleSite` folder and copy or cut all the elements, and now go back to the root folder and paste it here.

open the command prompt again and run `cd ../` command for go back to the root folder.

### STEP-4 : Host locally

Launching the website locally by using the following command:

```
hugo server
```

Go to `http://localhost:1313`


### STEP-5 : Basic configuration

When building the website, you can set a theme by using `--theme` option. However, we suggest you modify the configuration file (`config.toml`) and set the theme as the default.

```toml
# Change the default theme to be use when building the site with Hugo
theme = "navigator-hugo"
```

### STEP-6 : Create your first content pages

```
hugo new blog/post-name.md
```

### STEP-7 : Build the website

When your site is ready to deploy, run the following command:

```
hugo

# You can also create a minified version by using this command:
hugo--minify

```

A `public` folder will be generated, containing all static content and assets for your website. It can now be deployed on any web server.
