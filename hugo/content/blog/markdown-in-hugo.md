---
title: "Markdown Syntax in Hugo"
date: 2020-01-01T13:22:40+06:00
image: images/blog/0002.jpg
author: Derek Taylor
---

### Headings

Headings from h1 through h6 are constructed with a `#` for each level:

+ `# h1 Heading`
+ `## h2 Heading`
+ `### h3 Heading`
+ `#### h4 Heading`
+ `##### h5 Heading`
+ `###### h6 Heading`

&nbsp;
### Horizontal Rules

Horizontal rules are lines that create a nice visual break between sections of your content. In HTML, you use the `<hr>` tag to create the horizontal rule.  In markdown, you use one of the following:

+ `___ three consecutive underscores`
+ `--- three consecutive dashes`
+ `*** three consecutive asterisks`


&nbsp;
### Commenting

Comments can be made in the usual HTML-compatible way:

+ `<!--`
+ `This is a comment`
+ `-->`


&nbsp;
### Paragraphs

When you write a normal paragraph of plain text, such as this paragraph you are reading now, it will be wrapped in <p></p> tags when rendered as HTML.  In markdown, a new paragraph is created when you put one or more blank lines between them.  Line breaks can also be done by adding either a backslash `\` or two blank spaces at the end of the line.


&nbsp;
### Bold, Italics and Strikethrough

+ `**rendered as bold text**`
+ **rendered as bold text**

+ `_rendered as italicized text_`
+ _rendered as italicized text_

+ `~~Strike through this text.~~`
+ ~~Strike through this text.~~


&nbsp;
### Blockquotes

Blockquotes are great for quoting blocks of text from another source within your article.  Simply add a `>` before the block of text that you wish to quote. 


&nbsp;
### Lists

+ `* use an asterisk for an unordered list`
+ `+ or you can also use the plus symbol`
+ `- or a dash/minus character works as well`
