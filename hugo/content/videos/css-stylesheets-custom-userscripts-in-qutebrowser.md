---
title: "CSS Stylesheets And Custom Userscripts In Qutebrowser" 
image: images/thumbs/0748.jpg
date: 2020-11-05T12:23:40+06:00
author: Derek Taylor
tags: ["Qutebrowser", "GUI Apps"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/css-stylesheets-and-custom-userscripts/5813760b5914ae6e76993ea7a9dff640e73b594f?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Want a few more color scheme options for Qutebrowser?  I will show how you can add popular color themes like Solarized, Dracula and Gruvbox to Qutebrowser, and how to bind them so you can easily switch between them.  I also discuss custom userscripts in Qutebrowser.

REFERENCED:
+ https://github.com/alphapapa/solarized-everything-css - Solarized Everything CSS