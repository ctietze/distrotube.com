---
title: "Xubuntu 18.04 'Bionic Beaver' Install and Review"
image: images/thumbs/0187.jpg
date: Wed, 25 Apr 2018 22:48:08 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Xubuntu"]
---

#### VIDEO

{{< amazon src="Xubuntu+18.04+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at Xubuntu 18.04 "Bionic Beaver". The official release date is actually tomorrow so I'm reviewing this one day early, so this is technically a beta version but nothing should change between now and the release. https://xubuntu.org/
