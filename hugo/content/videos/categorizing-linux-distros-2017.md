---
title: "Categorizing Linux Distros - Which One Is Best For Beginners?"
image: images/thumbs/0007.jpg
date: Wed, 11 Oct 2017 01:45:45 +0000
author: Derek Taylor
tags: ["Linux History"]
---

#### VIDEO

{{< amazon src="Categorizing+Linux+Distros+-+Which+One+Is+Best+For+Beginners.mp4" >}}  
&nbsp;

#### SHOW NOTES

I break the hundreds of distros out there into two basic categories. And I make recommendations for the new Linux user.
