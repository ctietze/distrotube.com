---
title: "Free As In Freedom Friday - LIVE"
image: images/thumbs/0683.jpg
date: 2020-07-31T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Free+As+In+Freedom+Friday+-+LIVE-3m1stC7DL84.mp4" >}}
&nbsp;

#### SHOW NOTES

Just another random GNU/Linux-y live stream.  I will converse with the YouTube chat so feel free to post freedom-respecting questions and comments.  When posting questions and comments, ask yourself WWRD ("What would Richard do?).  Only freedom lovers allowed!