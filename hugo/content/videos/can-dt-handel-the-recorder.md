---
title: "Can DT Handel The Recorder?"
image: images/thumbs/0538.jpg
date: 2020-02-08T12:22:40+06:00
author: Derek Taylor
tags: ["Off Topic", ""]
---

#### VIDEO

{{< amazon src="Can+DT+Handel+The+Recorder.mp4" >}}
&nbsp;

#### SHOW NOTES

When I discuss playing a musical instrument to most "normies", they often respond with something like "the only instrument I learned to play was that cheap plastic recorder in elementary school."  Hey!  That plastic recorder is an instrument just like everyone else!

So, I decided to attempt this performance of Handel's Sonata in F Major, Op. 1.  The performance is far from perfect and I am limited by the non-human harpsichord accompaniment (couldn't afford to pay a real harpsichordist for this).  But I hope to demonstrate that a sub-$20 plastic instrument can do OK...in a pinch.

REFERENCED:
+  https://en.wikipedia.org/wiki/George_Frideric_Handel