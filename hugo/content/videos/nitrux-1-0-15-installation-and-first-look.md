---
title: "Nitrux 1.0.15 Installation and First Look"
image: images/thumbs/0271.jpg
date: Tue, 04 Sep 2018 18:22:05 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Nitrux"]
---

#### VIDEO

{{< amazon src="Nitrux+1.0.15+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Nitrux 1.0.15 was just released so I'm taking it for a quick spin in a 
virtual machine.  I've reviewed Nitrux before on the channel.  That 
review was quite negative.  Will this time be different?

<a href="https://www.youtube.com/redirect?redir_token=Tm1YxvrcB8M7GX7gdwxqMhzbbxZ8MTU1MzQ1MTc3MUAxNTUzMzY1Mzcx&amp;event=video_description&amp;v=AwYQhzuI0Jo&amp;q=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fnitruxos%2F" rel="noreferrer noopener" target="_blank">https://sourceforge.net/projects/nitr...
