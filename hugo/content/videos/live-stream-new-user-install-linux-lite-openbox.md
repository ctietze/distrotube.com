---
title: "Live Stream Jan 27, 2018 - New User Install Linux Lite and Openbox"
image: images/thumbs/0100.jpg
date: Sat, 27 Jan 2018 01:30:40 +0000
author: Derek Taylor
tags: ["Live Stream", "Linux Lite", "Openbox"]
---

#### VIDEO

{{< amazon src="Live+Stream+Jan+27%2C+2018+-+New+User+Install+Linux+Lite+and+Openbox.mp4" >}}  
&nbsp;

#### SHOW NOTES

I talk a Windows user through how to install Linux Lite in Virtualbox, and then we install Openbox in Linux Lite.
