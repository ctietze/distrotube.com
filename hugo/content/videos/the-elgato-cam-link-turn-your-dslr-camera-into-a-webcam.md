---
title: "The Elgato Cam Link - Turn Your DSLR Camera Into A Webcam"
image: images/thumbs/0289.jpg
date: Tue, 25 Sep 2018 19:45:51 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Elgato+Cam+Link+-+Turn+Your+DSLR+Camera+Into+A+Webcam.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been using the Elgato Cam Link for the past two months and I love this thing! I've used it mainly to capture the output from my Panasonic Lumix G7 camera--it basically turns a DSLR camera into a webcam. But the Cam Link can be used to capture any HDMI signal, such as from a Raspberry Pi or a gaming console.
