---
title: "Material Shell - Turn GNOME Into A Tiling Window Manager"
image: images/thumbs/0418.jpg
date: Fri, 05 Jul 2019 05:36:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "GNOME"]
---

#### VIDEO

{{< amazon src="Material+Shell+Turn+GNOME+Into+A+Tiling+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

You guys know I hate GNOME. But some of you thought that I might be impressed with the Material Shell, an extension that attempts to turn GNOME into a tiling window manager. So I took a very brief look at this alpha-quality software.
