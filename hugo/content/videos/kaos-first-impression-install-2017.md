---
title: "KaOS First Impression Install & Review"
image: images/thumbs/0028.jpg
date: Thu, 02 Nov 2017 02:32:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "KaOS", "KDE"]
---

#### VIDEO

{{< amazon src="KaOS+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at KaOS in this video. I run through the installation process and do a brief review of the desktop. KaOS is an independent Linux distro that uses the KDE desktop environment and uses the Pacman package manager.
