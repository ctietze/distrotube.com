---
title: "My Window Manager Configs Are A Mess"
image: images/thumbs/0567.jpg
date: 2020-03-12T12:22:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", ""]
---

#### VIDEO

{{< amazon src="My+Window+Manager+Configs+Are+A+Mess.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the things that I have been working on recently is cleaning up all of my tiling window manager configuration files. I know many people grab my configs from my GitLab, and I want to make sure that those people have a nice, consistent experience when trying out various window managers.

REFERENCED:
+ https://gitlab.com/dwt1 - My GitLab