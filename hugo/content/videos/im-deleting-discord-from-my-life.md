---
title: "I'm Deleting Discord From My Life" 
image: images/thumbs/0780.jpg
date: 2020-12-28T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/i-m-deleting-discord-from-my-life/af0d68fefb10f8e0e89952eb87ee3a2425c47bc1?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

I don't like to chat on the Internet.  I find it to be unproductive but more than that, I find it to be mundane and just not fun for me.  And because of my attitude towards Internet chat, I am never on my Discord channel or my IRC channel.  And this has caused me some headaches...