---
title: "Solus 4.1 'Fortitude' Budgie Edition"
image: images/thumbs/0527.jpg
date: 2020-01-27T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Solus", "Budgie"]
---

#### VIDEO

{{< amazon src="Solus+41+Fortitude+Budgie+Edition.mp4" >}}
&nbsp;

#### SHOW NOTES

One the distros that I often put on my laptops has been Solus Budgie.  I have found Solus to be a polished and stable distro.  Version 4.1 of Solus was just released.  How does it measure up?

REFERENCED:
+ https://getsol.us/2020/01/25/solus-4-1-released/