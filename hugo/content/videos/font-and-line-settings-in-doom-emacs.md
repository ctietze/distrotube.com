---
title: "Font And Line Settings In Doom Emacs" 
image: images/thumbs/0741.jpg
date: 2020-10-25T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/font-and-line-settings-in-doom-emacs/d7ee6aef204f3034e1ca4b0538e9958ab19a1fb2?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I am going to cover a few parts of my Doom Emacs config.   Specifically, I am going to talk about my settings for fonts and lines.  I also talk about what the fringe is and how you enable or disable the fringe.

REFERENCED:
+ https://github.com/hlissner/doom-emacs