---
title: "Using Nano Because Vim Is Scary? Use Micro Instead!"
image: images/thumbs/0646.jpg
date: 2020-06-16T12:23:40+06:00
author: Derek Taylor
tags: ["TUI Apps", ""]
---

#### VIDEO

{{< amazon src="Using+Nano+Because+Vim+Is+Scary+Use+Micro+Instead.mp4" >}}
&nbsp;

#### SHOW NOTES

Not leet enough to learn Vim or Emacs?  OK, but don't settle on being just another "nano noob."  There is a middle ground.  And that middle ground text editor is called micro!  It is a modern and intuitive terminal-based text editor that features syntax highlighting, splits, tabs, multiple cursors and a plugin system.  And it's easy to use!

REFERENCED:
+ https://micro-editor.github.io/