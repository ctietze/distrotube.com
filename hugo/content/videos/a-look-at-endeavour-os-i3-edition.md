---
title: "A Look At EndeavourOS i3 Edition"
image: images/thumbs/0598.jpg
date: 2020-04-16T12:23:40+06:00
author: Derek Taylor
tags: ["EndeavourOS", "i3", "Tiling Window Managers"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-look-at-endeavouros-i3-edition/e763a6723358c0fb5a6e3bfe4dd35792a757b07b?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

EndeavourOS is an Arch Linux-based distribution which features graphical install options and pre-configured desktop environments including a pre-configured i3, which I will take a look at. 

REFERENCED:
+ https://endeavouros.com/news/the-april-release-has-arrived/