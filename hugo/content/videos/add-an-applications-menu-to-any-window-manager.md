---
title: "Add An Applications Menu To Any Window Manager"
image: images/thumbs/0677.jpg
date: 2020-07-25T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "GUI Apps"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/add-an-applications-menu-to-any-window/0c59822bed771faa83978544dc55986511051b14?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Xmenu is a menu utility for X. Xmenu is a popup menu similar to the one found in Openbox, and it can be controlled via the mouse and/or via keyboard.   Perfect for window managers that don't include a menu application.

REFERENCED:
+ https://github.com/phillbush/xmenu