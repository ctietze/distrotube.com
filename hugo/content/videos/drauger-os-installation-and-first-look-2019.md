---
title: "Drauger OS Installation and First Look"
image: images/thumbs/0438.jpg
date: Mon, 09 Sep 2019 23:39:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Drauger OS"]
---

#### VIDEO

{{< amazon src="Drauger+OS+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I am taking a quick look at Drauger OS, an Ubuntu-based Linux distro that is billed as a gaming distro. Drauger OS uses a low latency kernel for improved gaming performance and comes with Steam, Lutris and PlayOnLinux.


&nbsp;
#### REFERENCED: 
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.draugeros.org%2Fgo%2F&amp;redir_token=EDL5Ys9fmytRjSe5I8KLUP9T6QZ8MTU3NzQ4OTk5MUAxNTc3NDAzNTkx&amp;event=video_description&amp;v=BSUbP2pUovU" target="_blank" rel="nofollow noopener noreferrer">https://www.draugeros.org/go/
