---
title: "Unfettered Freedom, Ep. 1 - Linux 5.8, Linux Libre, OpenSSF, LibreOffice, the Fediverse"
image: images/thumbs/0687.jpg
date: 2020-08-05T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", "FOSS Advocacy"]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom%2C+Episode+1.mp4" >}}
&nbsp;

#### SHOW NOTES

This is the inaugural episode of the Unfettered Freedom podcast, a show that focuses on news and topics about GNU/Linux, free software and open source software.  But more than the software, this podcast will focus on the "movement", the ideology and the freedom of being a part of this community! 

+ 0:00 - Intro
+ 1:11 - Kernel 5.8 is a big release for Linux...and for Linux-libre.
+ 5:45 - The Linux Foundation announces the Open Source Security Foundation.
+ 7:26 - LibreOffice 7.0 is released with a lot of nice features.
+ 10:10 - Invidio.us developer is quitting open source development.
+ 14:51 - Freedom respecting social sites and the Fediverse.
+ 20:10 - Final words and  a "thank you" to the Patrons! 

&nbsp;
##### AUDIO VERSION OF THIS PODCAST:
+ https://www.buzzsprout.com/1263722/episodes/4872818
+ https://open.spotify.com/episode/6S7VZGTBUPfqf3GjNi8Npq

&nbsp;
##### MUSIC ATTRIBUTION:
"Key To Your Heart" by The Mini Vandals (from the YT Audio Library)

##### CREATIVE COMMONS LICENSE:
This video is licensed with a Creative Commons CC BY license.  By marking this original video with a Creative Commons license, I am granting the community the right to reuse and edit that video.  Freedom, baby!