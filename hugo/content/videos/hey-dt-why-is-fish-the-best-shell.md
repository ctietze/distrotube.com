---
title: "'Hey, DT! Why Is Fish The Best Shell?' Plus Other Questions Answered." 
image: images/thumbs/0746.jpg
date: 2020-11-01T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/hey-dt-why-is-fish-the-best-shell-plus/91348e9af786e406bd6ebd3773e36bf3312e0096?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this lengthy rant video, I address a few questions and comments that I've been receiving from viewers.  Some of the topics include why I call Doom Emacs and Spacemacs "Emacs distributions", why I think Fish is the best shell, problems with YouTube recommendations, and how someone can learn to stop distro-hopping.