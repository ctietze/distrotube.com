---
title: "Vim Makes Everything Better, Especially Your File Manager And Shell!"
image: images/thumbs/0370.jpg
date: Wed, 20 Mar 2019 23:19:17 +0000
author: Derek Taylor
tags: ["terminal", "Vim", "vifm", "command line"]
---

#### VIDEO

{{< amazon src="Vim+Makes+Everything+Better+Especially+Your+File+Manager+And+Shell.mp4" >}}
&nbsp;

#### SHOW NOTES

The more I use Vim, the more I find myself wanting to do everything "the  Vim way."  In this video, I discuss vifm (a vim-like file manager) and I  show you how to enable Vi-mode in both the Bourne Again Shell (bash)  and the Z Shell (zsh). 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=QtcF6PYSPoaUEXvq6kbKGSYV6Lh8MTU1MzY0MjM3M0AxNTUzNTU1OTcz&amp;event=video_description&amp;v=a4scYdubKbo&amp;q=https%3A%2F%2Fvifm.info%2F" target="_blank">https://vifm.info/
