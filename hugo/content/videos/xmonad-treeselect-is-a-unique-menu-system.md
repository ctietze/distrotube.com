---
title: "Xmonad TreeSelect Is A Unique Menu System"
image: images/thumbs/0651.jpg
date: 2020-06-21T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Xmonad+TreeSelect+Is+A+Unique+Menu+System.mp4" >}}
&nbsp;

#### SHOW NOTES

I discovered this interesting Xmonad module called TreeSelect that displays your workspaces or a list of actions in a tree-like format. You can select the desired workspace/action with the mouse or with the HJKL keys.  This module is fully configurable and very useful if you like to have a lot of workspaces.

REFERENCED:
+ https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-TreeSelect.html