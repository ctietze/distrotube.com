---
title: "Bryan Lunduke's Decision to Lock Content Behind Patreon Paywall"
image: images/thumbs/0209.jpg
date: Thu, 17 May 2018 23:23:39 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Bryan+Lundukes+Decision+to+Lock+Content+Behind+Patreon+Paywall.mp4" >}}
&nbsp;

#### SHOW NOTES

Bryan Lunduke recently announced some big changes to his channel. His standard long form show will no longer be available to the general public but will only be available to his Patreon supporters. This is a rather lengthy rant about my thoughts regarding this decision. 
