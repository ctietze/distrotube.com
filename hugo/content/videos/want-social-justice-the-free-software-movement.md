---
title: "Want Social Justice? The Free Software Movement Fights For Everyone!"
image: images/thumbs/0693.jpg
date: 2020-08-13T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

{{< amazon src="Want+Social+Justice+The+Free+Software+Movement+Fights+For+Everyone.mp4" >}}
&nbsp;

#### SHOW NOTES

Everyone wants freedom but most people have no idea just how enslaved they have become to their computing devices and the proprietary software that controls those devices.  The Free Software Movement aims to spread awareness of this issue and to advocate for the use of freedom-respecting software ("free software"). 

I see too many people fighting against social injustices and trying to make a difference in the world.  Many of those people have chosen causes that, while noble causes, are not as wide-reaching as the Free Software Movement.  So if you truly want to affect change in the world, join the fight as we free the software!

JOIN THE FIGHT:
+ https://www.fsf.org/ - Free Software Foundation