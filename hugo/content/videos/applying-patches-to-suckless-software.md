---
title: "Applying Patches To Suckless Software" 
image: images/thumbs/0749.jpg
date: 2020-11-06T12:23:40+06:00
author: Derek Taylor
tags: ["Dmenu", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/applying-patches-to-suckless-software/15a2579c3a150023cce77350683b7977ba343aca?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I had a viewer ask me about patching Suckless software (like dwm, st and dmenu).  I have done videos on this in the past but it has been awhile.  And I noticed that my dmenu build is not up-to-date, so I'm killing two birds with one stone here.

#### NOTE:
If you want my dmenu build, go to my GitLab and "git clone" the dmenu-distrotube repo.  Then run "sudo make install".  For those of you on Arch, you can "yay dmenu-distrotube-git".

#### REFERENCED:
+ https://suckless.org/ - Suckless
+ https://gitlab.com/dwt1/dmenu-distrotube - DT's dmenu build