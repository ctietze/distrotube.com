---
title: "NixOS Is A Fascinating Linux Distribution" 
image: images/thumbs/0744.jpg
date: 2020-10-29T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "NixOS"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/nixos-is-a-fascinating-linux/55083c745694ae80ef17bab238e122475f023115?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

NixOS is a GNU/Linux distribution that employs a powerful package management system that allows you to build the system from a single configuration file.  NixOS supports atomic upgrades and rolling back to previous generations.  In theory, it should be an unbreakable system.

REFERENCED:
+ https://nixos.org/