---
title: "I Think I Like AppImages More Than Snaps And Flatpaks" 
image: images/thumbs/0736.jpg
date: 2020-10-19T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "TUI Apps"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/i-think-i-like-appimages-more-than-snaps/8071d133742a18dce23dc8510c3159ee2252cfd6?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

AppImages are self-contained applications which can simply be downloaded, made executable, and t run on almost any GNU/Linux distribution.   There ease-of-use has won me over.

#### REFERENCED:
+ https://www.appimagehub.com/ - AppImageHub
+ https://github.com/TheAssassin/AppImageLauncher - AppImageLauncher
+ https://www.appimagehub.com/p/1298369/ - AppImage Installer 
+ https://www.appimagehub.com/p/1355468/ - App Outlet