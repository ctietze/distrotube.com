---
title: "Taking Into Account, Ep. 42 - BTW Arch WSL, Contributing to FOSS, Zombieload, Indian Schools, Bing"
image: images/thumbs/0394.jpg
date: Thu, 16 May 2019 14:34:28 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+42.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
0:49 By the way, I use Arch Linux...on Windows 10. Arch is available on the WSL.
+ 4:45 Eight ways to contribute to the Linux community, without knowing a single line of code.
+ 13:24 Zombieload, an Intel processor side-channel attack, poses a security threat for Linux systems.
+ 18:26 Schools in Indian state of Kerala are expected to save $428 million by choosing Linux.
+ 22:29 Microsoft open sources algorithm that gives Bing some of its smarts.
+ 26:34 I read a viewer comment from Mastodon regarding "bloat".


&nbsp;
#### REFERENCED:
+ https://betanews.com/2019/05/12/btw-arch-linux-windows-10/
+ https://www.forbes.com/sites/jasonevangelho/2019/05/13/8-ways-contribute-desktop-linux-community-marketing-documentation-art-testing/
+ https://www.zdnet.com/article/linux-vs-zombieload/
+ https://itsfoss.com/kerala-linux/
+ https://arstechnica.com/gadgets/2019/05/microsoft-open-sources-algorithm-that-gives-bing-some-of-its-smarts/
