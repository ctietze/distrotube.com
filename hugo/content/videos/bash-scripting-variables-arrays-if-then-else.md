---
title: "Bash Scripting - Variables, Arrays, If-Then-Else"
image: images/thumbs/0451.jpg
date: Sat, 26 Oct 2019 00:00:00 +0000
author: Derek Taylor
tags: ["shell", "shell scripting", "command line"]
---

#### VIDEO

{{< amazon src="Bash+Scripting+Variables+Arrays+If-Then-Else.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to cover a few of the fundamentals of bash scripting. I will make a quick and dirty bash script as an example.

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=CojOZbP-0GX4Tjt0U-XjLqf1JoF8MTU3NzQ5MTI2M0AxNTc3NDA0ODYz&amp;q=https%3A%2F%2Fwww.gnu.org%2Fsoftware%2Fbash%2F&amp;event=video_description&amp;v=xhI1qXUrAHw" target="_blank" rel="nofollow noopener noreferrer">https://www.gnu.org/software/bash/
