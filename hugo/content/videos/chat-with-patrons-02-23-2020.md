---
title: "Chat With Patrons (February 23, 2020)"
image: images/thumbs/0552.jpg
date: 2020-02-23T12:22:40+06:00
author: Derek Taylor
tags: ["Chat With Patrons", "Live Stream"]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(February+23%2C+2020)-JEIj3LvJ6yI.mp4" >}}
&nbsp;

#### SHOW NOTES

This Video Chat will be for my Patrons!  Patrons can join the video call via Zoom which is available on Linux, Mac and Windows.  For those wishing to join the chat but are not a Patron, consider joining my Patreon ( https://www.patreon.com/distrotube ).  I will post a link to the Zoom chat on my Patreon page a few minutes prior to the stream.  Hope to see you guys there!

REFERENCED:
+ https://www.patreon.com/distrotube - My Patreon