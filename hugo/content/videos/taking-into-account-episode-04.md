---
title: "Taking Into Account, Ep. 4 - AMD, Intel, Debian, Dropbox, Linux Gaming"
image: images/thumbs/0256.jpg
date: Thu, 16 Aug 2018 22:36:15 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+4+-+AMD%2C+Intel%2C+Debian%2C+Dropbox%2C+Linux+Gaming.mp4" >}}
&nbsp;

#### SHOW NOTES

+ 0:50 AMD's 32-core threadripper better on Linux than Windows 
+ 8:24 The L1 Terminal Fault aka Foreshadow 
+ 13:16 Happy 25th birthday, Debian! 
+ 19:32 Dropbox dropping support for most Linux filesystems 
+ 26:18 Game reviews helps developers 
+ 31:25 I read a question from one of the viewers
