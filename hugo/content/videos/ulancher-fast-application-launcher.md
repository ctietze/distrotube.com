---
title: "Ulauncher Fast Application Launcher"
image: images/thumbs/0070.jpg
date: Wed, 20 Dec 2017 15:49:51 +0000
author: Derek Taylor
tags: ["Command Launcher", "ulauncher"]
---

#### VIDEO

{{< amazon src="Ulauncher+Fast+Application+Launcher.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a brief look at Ulauncher, a fast application launcher written in Python and using the GTK toolkit. Released under the GPL v3. <a href="https://github.com/Ulauncher/Ulauncher">https://github.com/Ulauncher/Ulauncher</a>
