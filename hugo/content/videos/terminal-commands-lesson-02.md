---
title: "Terminal Commands Lesson 02 - touch, mkdir, mv, cp, rm, rmdir"
image: images/thumbs/0003.jpg
date: Sun, 08 Oct 2017 01:39:48 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Terminal+Commands+Lesson+02+-+touch%2C+mkdir%2C+mv%2C+cp%2C+rm%2C+rmdir.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this part of our terminal commands series of videos, we discuss how to make, move, copy and delete files and directories using the following commands: touch, mkdir, mv, cp, rm, rmdir.
