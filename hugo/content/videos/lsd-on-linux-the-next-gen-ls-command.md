---
title: "LSD on Linux - the next gen 'ls' command"
image: images/thumbs/0361.jpg
date: Sun, 03 Mar 2019 23:08:12 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="LSD+on+Linux+the+next+gen+ls+command.mp4" >}}
&nbsp;

#### SHOW NOTES

LSD (LSDeluxe) is the next gen "ls" command.  It adds some neat colors to "ls" as well as unicode character icons. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2FPeltoche%2Flsd&amp;v=cz25mFcAOwU&amp;event=video_description&amp;redir_token=Ff2xAtD9GPXJxNh7E0Jt0To9zb18MTU1MzY0MTY5NkAxNTUzNTU1Mjk2" target="_blank">https://github.com/Peltoche/lsd 
