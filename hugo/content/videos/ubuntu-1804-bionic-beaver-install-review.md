---
title: "Ubuntu 18.04 LTS 'Bionic Beaver' Install and Review"
image: images/thumbs/0188.jpg
date: Thu, 26 Apr 2018 22:49:03 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

{{< amazon src="Ubuntu+18.04+LTS+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at the flagship edition--Ubuntu 18.04 "Bionic Beaver". Ubuntu 18.04 sports the GNOME desktop environment and offers support for up to five years. https://www.ubuntu.com/
