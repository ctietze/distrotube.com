---
title: "30 Day Challenge - Tiling Windows and TUI Programs"
image: images/thumbs/0173.jpg
date: Tue, 10 Apr 2018 22:25:56 +0000
author: Derek Taylor
tags: ["tiling window managers", "terminal", "command line"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/30-day-challenge-tiling-windows-and-tui/4cbd71b65f031d4fce6f8a0b60f1e8617b4938cd?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

For the next 30 days, I'm giving myself a challenge--live in a tiling window manager and use only terminal-based applications (where possible). What will be the result of this experiment? 
