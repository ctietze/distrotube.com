---
title: "Notion Is A Tiling and Tabbing Window Manager" 
image: images/thumbs/0711.jpg
date: 2020-09-10T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "notion"]
---

#### VIDEO

{{< amazon src="Notion+Is+A+Tiling+and+Tabbing+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Notion is a tiling, tabbed window manager for the X11.  It is a static window manager as opposed to a dynamic window manager, and it is configurable using Lua.  If you are one of those i3 users that love that window manager because of its tabbing layout, then you may want to try out notion.

REFERENCED:
+ https://notionwm.net/