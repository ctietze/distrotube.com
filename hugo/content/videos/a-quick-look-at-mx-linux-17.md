---
title: "A Quick Look at the Official Release of MX Linux 17"
image: images/thumbs/0064.jpg
date: Sun, 17 Dec 2017 15:22:13 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MX Linux", "XFCE"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-the-official-release-of/a296c7b2dee59c53e5a0b7fee6d3f2df3b189754?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

About a month ago, I took a look the beta version of MX Linux 17 and loved it. In this video, I take a brief look at the official release of MX 17. <a href="https://mxlinux.org/">https://mxlinux.org/</a>
