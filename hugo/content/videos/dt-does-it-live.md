---
title: "DT Does It Live"
image: images/thumbs/0701.jpg
date: 2020-08-25T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Emacs"]
---

#### VIDEO

{{< amazon src="DT+Does+It+Live-iYIkLCYSvLs.mp4" >}}
&nbsp;

#### SHOW NOTES

A random live stream to see if I can live stream since I recently distrohopped.  I might talk a little about GNU/Linux and GNU/Life.  So hangout and ask questions.  If they aren't completely asinine, I might attempt to answer them.: