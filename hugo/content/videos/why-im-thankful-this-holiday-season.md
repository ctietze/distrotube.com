---
title: "Why I'm Thankful This Holiday Season"
image: images/thumbs/0309.jpg
date: Wed, 21 Nov 2018 00:30:58 +0000
author: Derek Taylor
tags: ["Public Service Announcements", ""]
---

#### VIDEO

{{< amazon src="What+I'm+Thankful+This+Holiday+Season+2.mp4" >}}
&nbsp;

#### SHOW NOTES

It's Thanksgiving in the United States.  It is a time of reflection and 
giving thanks for the blessings in one's life.  Here are few things that
 I am thankful for!

MUSIC:
"Heavenly" by Aakash Gandhi ( <a href="https://www.youtube.com/user/88keystoeuphoria/videos">https://www.youtube.com/user/88keysto... )
The track can be found in the YouTube Audio Library.
