---
title: "Simplifying the Shell with Bash Aliases"
image: images/thumbs/0164.jpg
date: Sat, 31 Mar 2018 22:14:17 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Simplifying+the+Shell+with+Bash+Aliases.mp4" >}}  
&nbsp;

#### SHOW NOTES

Tired of typing those long and convoluted commands in the shell? Here's how to make short and remember-able aliases for them. 
