---
title: "Doom Emacs And EXWM Are My New Window Manager" 
image: images/thumbs/0758.jpg
date: 2020-11-19T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", "exwm"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/doom-emacs-and-exwm-are-my-new-window/b9b967f2ac9b1995e56e0361c98500750b4764c9?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

A few months ago, I took a brief look at EXWM which is an Emacs plugin that allows you to use Emacs as your window manager.  And while I was impressed at how well EXWM can work as a window manager, I never really tried to live in it.  But I think I'm ready to give it a shot!

REFERENCED:
+ https://github.com/hlissner/doom-emacs - Doom Emacs
+ https://github.com/ch11ng/exwm - EXWM

