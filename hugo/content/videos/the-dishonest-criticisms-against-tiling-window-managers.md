---
title: "The Dishonest Criticisms Against Tiling Window Managers" 
image: images/thumbs/0721.jpg
date: 2020-09-23T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", ""]
---

#### VIDEO

{{< amazon src="The+Dishonest+Criticisms+Against+Tiling+Window+Managers.mp4" >}}
&nbsp;

#### SHOW NOTES

I've noticed that anytime I do a video about window managers, especially tiling window managers, that I get a lot of comments about how people shouldn't waste their time with window managers and that it isn't worth the effort involved.  I think those arguments are dishonest and I want to address them. 