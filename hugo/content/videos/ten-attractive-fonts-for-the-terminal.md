---
title: "Ten Attractive Fonts For Your Terminal Or Text Editor"
image: images/thumbs/0632.jpg
date: 2020-05-27T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", ""]
---

#### VIDEO

{{< amazon src="Ten+Attractive+Fonts+For+Your+Terminal+Or+Text+Editor.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to briefly show you ten different monospaced fonts that are great for your terminal and/or text editor.  These fonts are freely available and can be found in most Linux distro's repositories.  