---
title: "Rofi Is Like Dmenu But Sucks Less" 
image: images/thumbs/0771.jpg
date: 2020-12-10T12:23:40+06:00
author: Derek Taylor
tags: ["Rofi", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/rofi-is-like-dmenu-but-sucks-less/05be443821dfff3538bdfc78eca4db4e97f74daa?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

Rofi is a run launcher similar to dmenu but it comes with more configuration options without the hassle of patching.  Rofi, like dmenu, will provide the user with a textual list of options where one or more can be selected. This can either be running an application, selecting a window, or options provided by an external script.

REFERENCED:
+ https://github.com/davatorium/rofi - Rofi