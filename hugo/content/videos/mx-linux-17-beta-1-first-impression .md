---
title: "MX Linux 17 Beta 1 First Impression Install & Review"
image: images/thumbs/0032.jpg
date: Mon, 13 Nov 2017 14:07:46 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MX Linux", "XFCE"]
---

#### VIDEO

{{< amazon src="MX+Linux+17+Beta+1+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I review MX Linux, a collaboration of the old MEPIS distro and the AntiX distro. It is based on Debian stable and uses the XFCE desktop environment.
