---
title: "ROSA R10 Fresh KDE First Impression Install & Review"
image: images/thumbs/0053.jpg
date: Thu, 07 Dec 2017 14:45:38 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ROSA", "KDE"]
---

#### VIDEO

{{< amazon src="ROSA+R10+Fresh+KDE+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I take a look at Russian Linux distribution named ROSA. An independent distro that was originally based on Mandriva, ROSA delivers an easy installation process and a highly customized KDE desktop. <a href="http://www.rosalab.com/">http://www.rosalab.com/</a>
