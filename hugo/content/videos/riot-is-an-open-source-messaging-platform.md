---
title: "Riot Is An Open Source Messaging Platform - DT LIVE"
image: images/thumbs/0601.jpg
date: 2020-04-19T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Riot+Is+An+Open+Source+Messaging+Platform+-+DT+LIVE-o6-MywLVhP8.mp4" >}}
&nbsp;

#### SHOW NOTES

Riot is an open source messaging platform that offers text-chat, private messaging, voice chat and video chat.  It's a free (as in freedom) Discord alternative.

REFERENCED:
+ https://about.riot.im/
