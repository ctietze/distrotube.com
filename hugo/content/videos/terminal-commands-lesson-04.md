---
title: "Terminal Commands Lesson 04 - Editing Text Files - echo, cat, nano, vi"
image: images/thumbs/0026.jpg
date: Sun, 29 Oct 2017 02:30:33 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Terminal+Commands+Lesson+04+-+Editing+Text+Files+-+echo%2C+cat%2C+nano%2C+vi.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I show you how to create and edit text documents from the terminal. The commands covered include the cat and echo commands as well as the text editors nano and vi(m).
