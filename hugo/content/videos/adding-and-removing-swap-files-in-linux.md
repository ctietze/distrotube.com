---
title: "Adding And Removing Swap Files Is Easy In Linux" 
image: images/thumbs/0718.jpg
date: 2020-09-20T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/adding-and-removing-swap-files-is-easy/c7250fe7f21aaa3c2acba806f7e2e49a4a104fac?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this quick tutorial, I am going to create a swap file on my Linux desktop.  I'm also going to show you how to remove a swap file.  Below are the commands that I used in this video:
 
#### ADDING SWAP FILE
+ sudo dd if=/dev/zero of=/swapfile bs=1M count=1024
+ sudo chmod 600 /swapfile
+ sudo mkswap /swapfile
+ sudo swapon /swapfile
+ swapon --show
+ Edit  /etc/fstab and add:
+ /swapfile none swap defaults 0 0

#### REMOVING SWAP FILE
+ sudo swapoff -v /swapfile
+ sudo rm /swapfile
+ Edit /etc/fstab and remove the entry for the swapfile.