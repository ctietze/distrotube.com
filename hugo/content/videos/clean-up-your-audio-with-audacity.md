---
title: "Clean Up Your Audio With Audacity"
image: images/thumbs/0465.jpg
date: Sun, 17 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["GUI Apps", "Audio"]
---

#### VIDEO

{{< amazon src="Clean+Up+Your+Audio+With+Audacity.mp4" >}}
&nbsp;

#### SHOW NOTES

Audacity is an easy-to-use, multi-track audio editor and recorder for Windows, Mac OS X, GNU/Linux and other operating systems. Developed by a group of volunteers as open source.

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=5RhmZUTEtzc&amp;redir_token=f_SwpnjGatteScSZrgQLHZ-eBr58MTU3NzQ5MjQ1N0AxNTc3NDA2MDU3&amp;event=video_description&amp;q=https%3A%2F%2Fwww.audacityteam.org%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.audacityteam.org/
