---
title: "Archbang (systemd) Install & Review"
image: images/thumbs/0071.jpg
date: Sat, 22 Dec 2017 15:52:26 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archbang-systemd-install-review/5e59d870654b816e6829cb6dc966c34a26187969?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today I install and review Archbang (systemd).  It is an Arch-based Linux distro that uses the Openbox window manager, tint2 panel and conky system monitor.  It is fast and lightweight. http://bbs.archbang.org/
