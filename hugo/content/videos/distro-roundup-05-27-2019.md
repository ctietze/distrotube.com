---
title: "Distro Roundup (May 27, 2019) - TAILS, Kali, OpenSUSE, BlackArch"
image: images/thumbs/0404.jpg
date: Mon, 27 May 2019 17:40:24 +0000
author: Derek Taylor
tags: ["Distro Roundup", "TAILS", "Kali", "OpenSUSE", "BlackArch"]
---

#### VIDEO

{{< amazon src="Distro+Roundup+May+27+2019.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Distro Roundup:
+ 0:17 TAILS 3.14
+ 9:22 Kali Linux 2019.2
+ 13:59 OpenSUSE 15.1
+ 19:26 BlackArch 2019.06.01


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftails.boum.org%2Fnews%2Fversion_3.14%2Findex.en.html&amp;event=video_description&amp;v=VS2dO-fDW1k&amp;redir_token=2lh_AzoyXuaR1EICVgb7Mjqz0kp8MTU2NjQ5NTY3NEAxNTY2NDA5Mjc0" target="_blank" rel="nofollow noopener noreferrer">https://tails.boum.org/news/version_3...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.kali.org%2Fnews%2Fkali-linux-2019-2-release%2F&amp;event=video_description&amp;v=VS2dO-fDW1k&amp;redir_token=2lh_AzoyXuaR1EICVgb7Mjqz0kp8MTU2NjQ5NTY3NEAxNTY2NDA5Mjc0" target="_blank" rel="nofollow noopener noreferrer">https://www.kali.org/news/kali-linux-...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fnews.opensuse.org%2F2019%2F05%2F22%2Fopensuse-community-releases-leap-15-1-version%2F&amp;event=video_description&amp;v=VS2dO-fDW1k&amp;redir_token=2lh_AzoyXuaR1EICVgb7Mjqz0kp8MTU2NjQ5NTY3NEAxNTY2NDA5Mjc0" target="_blank" rel="nofollow noopener noreferrer">https://news.opensuse.org/2019/05/22/...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fblackarch.org%2Fblog.html&amp;event=video_description&amp;v=VS2dO-fDW1k&amp;redir_token=2lh_AzoyXuaR1EICVgb7Mjqz0kp8MTU2NjQ5NTY3NEAxNTY2NDA5Mjc0" target="_blank" rel="nofollow noopener noreferrer">https://blackarch.org/blog.html
