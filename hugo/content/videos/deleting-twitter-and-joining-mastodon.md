---
title: "Deleting Twitter and Joining Mastodon"
image: images/thumbs/0201.jpg
date: Wed, 09 May 2018 23:06:24 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Deleting+Twitter+and+Joining+Mastodon.mp4" >}}
&nbsp;

#### SHOW NOTES

Facebook does not respect the privacy of its users. So I deleted my account last week (made a video about it). Moved from Facebook to Diaspora. This week I'm deleteing Twitter. I want it out of my life as well--for privacy reasons. Moving from Twitter over to Mastodon. If you guys want to join me... 
DIASPORA: https://diasporafoundation.org/
MASTODON: https://mastodon.technology
