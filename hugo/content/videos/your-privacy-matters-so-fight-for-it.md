---
title: "Your Privacy Matters, So Fight For It"
image: images/thumbs/0529.jpg
date: 2020-01-29T12:22:40+06:00
author: Derek Taylor
tags: ["Privacy", "", ""]
---

#### VIDEO

{{< amazon src="Your+Privacy+Matters%2C+So+Fight+For+It.mp4" >}}
&nbsp;

#### SHOW NOTES

I see far too many people today, especially the younger generation, that do not take privacy seriously.  They assume that they are always being watched and listened to, and they are OK with that.  Your right to privacy is not something that you should be so willing to compromise.

REFERENCED:
+ https://www.fsf.org/campaigns/surveillance