---
title: "'Hey DT, Ever Used Emacs To Defeat A Superhero?' (Plus Other Questions Answered)"
image: images/thumbs/0675.jpg
date: 2020-07-22T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

{{< amazon src="Hey+DT+Ever+Used+Emacs+To+Defeat+A+Superhero+Plus+Other+Questions+Answered.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions and comments that I've been receiving from viewers.  I discuss differences between the terminal and the shell, why both Mint and Arch are appropriate "beginner" distros, and why I advocate being anonymous online yet post public videos on YouTube.  Plus I get a cheeky comment about Emacs and my resemblance to a super villain.