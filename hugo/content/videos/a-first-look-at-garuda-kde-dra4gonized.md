---
title: "A First Look At Garuda Linux KDE Dr4Gonized" 
image: images/thumbs/0761.jpg
date: 2020-11-24T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Garuda", "KDE"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-first-look-at-garuda-linux-kde/bad9658e5c8282c18aa100bac3321538fbd23ce5?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Recently I have been getting a lot of requests from viewers of the channel to take a look at a Linux distribution that I had never heard about--Garuda Linux.  This is yet another Arch-based distro but it does some different things.  Most notable is that it uses the Zen kernel and seems to be optimized for gamers.  

REFERENCED:
+ https://garudalinux.org/ - Garuda Linux

