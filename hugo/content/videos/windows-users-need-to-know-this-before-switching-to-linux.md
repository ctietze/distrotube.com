---
title: "Windows Users Need To Know This Before Switching To Linux"
image: images/thumbs/0558.jpg
date: 2020-02-29T12:22:40+06:00
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Windows+Users+Need+To+Know+This+Before+Switching+To+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you thinking about switching to Linux from Windows or Mac?  If so, there are some things you really need to know before making the jump into Linux.  Otherwise, you are going to be in for some major surprises when you start using Linux.