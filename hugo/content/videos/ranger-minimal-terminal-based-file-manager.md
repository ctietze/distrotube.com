---
title: "Ranger - A Minimal Terminal-Based File Manager"
image: images/thumbs/0034.jpg
date: Fri, 17 Nov 2017 14:13:18 +0000
author: Derek Taylor
tags: ["TUI Apps", "ranger", "file manager"]
---

#### VIDEO

{{< amazon src="Ranger+-+A+Minimal+Terminal-Based+File+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I take a look at Ranger, the lightweight file manager that you run inside your terminal. It is a perfect file manager for those of you that live inside the terminal, or for power users that want to leverage the power of Ranger's customization. <a href="http://ranger.nongnu.org/">http://ranger.nongnu.org/</a>
