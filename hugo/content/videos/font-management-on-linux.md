---
title: "Font Management On Linux" 
image: images/thumbs/0767.jpg
date: 2020-12-04T12:23:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/font-management-on-linux/d251893bdbef0aee76d15ad4b1fcbf6fe318b02f?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Many new-to-Linux users have questions about installing fonts and previewing fonts on Linux.  While there are some nice GUI applications that help with these tasks, you don't actually need to install any extra programs to manage your fonts.  Everything you need is already installed on your system.