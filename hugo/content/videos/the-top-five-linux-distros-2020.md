---
title: "The Top Five Linux Distros Of 2020" 
image: images/thumbs/0755.jpg
date: 2020-11-14T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-top-five-linux-distros-of-2020/6966bc43a7247af0161679cb517f8d95e2f684fe?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

In 2020, I took a look at many Linux distributions. Some of them really impressed me. So as the year nears the end, what are my "Top Five" distros for 2020? The answers may surprise you.

