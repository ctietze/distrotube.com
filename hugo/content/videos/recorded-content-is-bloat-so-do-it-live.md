---
title: "Recorded Content Is Bloat, So Let's Do It LIVE!"
image: images/thumbs/0625.jpg
date: 2020-05-17T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Recorded+Content+Is+Bloat%2C+So+Let's+Do+It+LIVE!-xkY7RtASL4w.mp4" >}}
&nbsp;

#### SHOW NOTES

This will be a random live stream without any topic.  Feel free to ask questions in the YouTube chat.  If your question isn't completely asinine, I might try to answer it.  I make no promises though.