---
title: "My Workflow - Tiling Window Managers, Workspaces and Multi-Monitors"
image: images/thumbs/0440.jpg
date: Thu, 12 Sep 2019 23:43:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "workflow"]
---

#### VIDEO

{{< amazon src="My+Workflow+Tiling+Window+Managers+Workspaces+and+Multi+Monitor.mp4" >}}
&nbsp;

#### SHOW NOTES

I am often asked about my workflow. Here's a quick video about how I typically work on my computer.

REFERENCED:

+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FWorkflow&amp;v=Q7j0-FMEya4&amp;redir_token=sW13W2pyUZwuAgntdp9xZOsP8eJ8MTU3NzQ5MDIwOEAxNTc3NDAzODA4&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://en.wikipedia.org/wiki/Workflow
