---
title: "Taking Into Account, Ep. 18 - Linux in 2019, Malware, Windows 10, YouTube, LibreHunt"
image: images/thumbs/0311.jpg
date: Thu, 29 Nov 2018 19:15:50 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+18.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=55s">0:55 Linux gets better every year.  What to expect of Linux in 2019? 

<a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=508s">8:28 Viruses and malware in Linux.  We are seeing more of these. 

<a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=890s">14:50 The alternatives to Windows 10 if you are tired of broken updates.

 <a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=1416s">23:36 Bring in the ads! YouTube may let everyone watch original shows for free. 

<a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=1726s">28:46 Discover the perfect Linux distro using this online tool. 

<a href="https://www.youtube.com/watch?v=XdbFdMJYSeg&amp;t=2170s">36:10 I answer a question from the viewers. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Fwww.networkworld.com%2Farticle%2F3323390%2Flinux%2Fwhat-to-expect-of-linux-in-2019.html&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://www.networkworld.com/article/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Fmedium.com%2Fintrinsic%2Fcompromised-npm-package-event-stream-d47d08605502&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://medium.com/intrinsic/compromi...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Fwww.theinquirer.net%2Finquirer%2Fnews%2F3066979%2Fthis-linux-virus-is-a-total-jerk-even-by-malware-standards&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://www.theinquirer.net/inquirer/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Fmybroadband.co.za%2Fnews%2Fsoftware%2F287252-the-alternatives-to-windows-10-if-you-are-tired-of-broken-updates.html&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://mybroadband.co.za/news/softwa...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Farstechnica.com%2Fgadgets%2F2018%2F11%2Fyoutube-may-make-originals-free-with-ads-for-all-to-watch-by-2020%2F&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://arstechnica.com/gadgets/2018/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hmEXf97G0iSCcrVn9mH6khj5Upd8MTU1MzU0MTUzMUAxNTUzNDU1MTMx&amp;q=https%3A%2F%2Flibrehunt.org%2F&amp;event=video_description&amp;v=XdbFdMJYSeg" target="_blank">https://librehunt.org/
