---
title: "Switching To The Alacritty Terminal Emulator"
image: images/thumbs/0532.jpg
date: 2020-02-01T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Switching+To+The+Alacritty+Terminal+Emulator.mp4" >}}
&nbsp;

#### SHOW NOTES

Alacritty is a free and open source terminal emulator written in Rust.  It is cross-platform and supports GNU/Linux, BSD, MacOS and Windows.  It uses GPU acceleration and claims to be the fastest terminal emulator ever.  It is also quite customizable.  Because of these points, I think I will switch to using Alacritty as my terminal emulator.

REFERENCED:
+ https://github.com/alacritty/alacritty