---
title: "Vim Versus Emacs. Which Is Better?"
image: images/thumbs/0537.jpg
date: 2020-02-07T12:22:40+06:00
author: Derek Taylor
tags: ["Vim", "Emacs"]
---

#### VIDEO

{{< amazon src="Vim+Versus+Emacs+Which+Is+Better.mp4" >}}
&nbsp;

#### SHOW NOTES

It is the question of all questions--vim or emacs?  The Editor Wars have raged for decades, Vimmers versus Emacsians, with the occasional Nano fanboy getting caught in the crossfire.  Some of you guys want to know which of these editors I think is better.  Here are some of my thoughts.

REFERENCED:
+  https://www.vim.org/ - Vim
+  https://www.gnu.org/software/emacs/ - GNU Emacs
+  https://github.com/hlissner/doom-emacs - Doom Emacs