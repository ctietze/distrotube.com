---
title: "A Look at GParted Live 0.31.0-1"
image: images/thumbs/0166.jpg
date: Mon, 02 Apr 2018 22:17:12 +0000
author: Derek Taylor
tags: ["Distro Reviews", "GParted Live"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-look-at-gparted-live-0-31-0-1/73aa8148afa158e0d6e73a9f3c6c86c1ae5a5263?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I take a quick look at a live Linux distribution called GParted Live. It is based on Debian Sid and is designed to be a live system rescue CD or USB stick. <a href="https://gparted.org/livecd.php">https://gparted.org/livecd.php</a> 
