---
title: "An Introduction to DistroTube"
image: images/thumbs/0453.jpg
date: Tue, 29 Oct 2019 00:03:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/an-introduction-to-distrotube/3a296d03e90a58d7bc8c83489299db84399edc3c?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

This video serves as an introduction to my channel. An introduction video is useful to have posted on your YouTube channel and your Patreon.
