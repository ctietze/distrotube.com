---
title: "BadWolf Is A Minimal, Privacy-Oriented Web Browser"
image: images/thumbs/0658.jpg
date: 2020-07-02T12:23:40+06:00
author: Derek Taylor
tags: ["Privacy and Security", ""]
---

#### VIDEO

{{< amazon src="BadWolf+Is+A+Minimal+Privacy-Oriented+Web+Browser.mp4" >}}
&nbsp;

#### SHOW NOTES

BadWolf is a minimalist and privacy-oriented WebKitGTK+ browser.  I've been looking for a good minimal web browser for a long time now.  And BadWolf might be the best one that I've tried.  

REFERENCED:
+ https://hacktivis.me/projects/badwolf - BadWolf Site
+ https://hacktivis.me/git/badwolf.mdoc/ - BadWolf Manpage