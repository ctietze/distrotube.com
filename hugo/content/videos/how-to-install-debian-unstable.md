---
title: "How To Install Debian Unstable"
image: images/thumbs/0073.jpg
date: Tue, 26 Dec 2017 15:54:05 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Debian"]
---

#### VIDEO

{{< amazon src="How+To+Install+Debian+Unstable.mp4" >}} 
&nbsp;

#### SHOW NOTES

How do you get a fresh install of Debian Unstable (Sid)? Well, there are three routes you could take. I show you my preferred method of installing Sid. <a href="https://wiki.debian.org/InstallFAQ">https://wiki.debian.org/InstallFAQ </a><a href="http://ftp.us.debian.org/debian/dists/unstable/main/installer-amd64/current/images/netboot/mini.iso">ftp.us.debian.org/debian/dists/unstable/main/installer-amd64/current/images/netboot/mini.iso</a>
