---
title: "The Scariest Movie Of This Year Or Any Year" 
image: images/thumbs/0745.jpg
date: 2020-10-30T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-scariest-movie-of-this-year-or-any/37d2031c26c4babbe0daa32ad1507f62f9862e68?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Coming to a computer near you, a film that is horrifying and reprehensible.  And the story is all too real!  It will make the ARCH movie look like "My Little Pony"!