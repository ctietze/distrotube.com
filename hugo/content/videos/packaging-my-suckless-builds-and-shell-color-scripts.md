---
title: "Packaging My Suckless Builds And Shell Color Scripts"
image: images/thumbs/0574.jpg
date: 2020-03-20T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "dwm"]
---

#### VIDEO

{{< amazon src="Packaging+My+Suckless+Builds+And+Shell+Color+Scripts.mp4" >}}
&nbsp;

#### SHOW NOTES

Thanks to having some extra free time, due to the quarantine, I am finally getting around to doing some of the things that I have wanted to do for some time.  I have packaged my personal builds of dwm and st and placed them in the AUR for those of you on Arch-based distros.  I also worked a bit on my shell-color-scripts repository.

INSTALL THESE FROM THE AUR WITH YAY:
+ yay -S dwm-distrotube-git
+ yay -S st-distrotube-git
+ yay -S shell-color-scripts