---
title: "The Top 8 Linux Window Managers of 2020" 
image: images/thumbs/0766.jpg
date: 2020-12-01T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-top-8-linux-window-managers-of-2020/ab528b9f324da0cb43d41337653e8e3877568ff5?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

So many people do their "top distros" list or a "top apps" list.  It's past time that someone did a "top window managers" list.  So here is my top eight window managers that are available on Linux.
