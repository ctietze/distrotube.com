---
title: "Linus Leaves Linux, A New Code Of Conduct and Community In Conflict"
image: images/thumbs/0278.jpg
date: Mon, 17 Sep 2018 18:54:57 +0000
author: Derek Taylor
tags: ["Linus Torvalds", ""]
---

#### VIDEO

{{< amazon src="Linus+Leaves+Linux%2C+A+New+Code+Of+Conduct+and+Community+In+Conflict.mp4" >}}
&nbsp;

#### SHOW NOTES

Major news the last couple of days.  Linus Torvalds has announced that  he is taking a break from his work on the kernel.  Plus, there is a new  Code of Conduct for kernel developers.  These two stories have caused a  lot of bickering inside the open source community. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8gHXQo2gYTlFOvkv6DIBKXdzmz18MTU1MzQ1MzcxOEAxNTUzMzY3MzE4&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinus-torvalds-takes-a-break-from-linux%2F&amp;v=y3CbAAwG8dk&amp;event=video_description" target="_blank">https://www.zdnet.com/article/linus-t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8gHXQo2gYTlFOvkv6DIBKXdzmz18MTU1MzQ1MzcxOEAxNTUzMzY3MzE4&amp;q=https%3A%2F%2Flkml.org%2Flkml%2F2018%2F9%2F16%2F167&amp;v=y3CbAAwG8dk&amp;event=video_description" target="_blank">https://lkml.org/lkml/2018/9/16/167

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8gHXQo2gYTlFOvkv6DIBKXdzmz18MTU1MzQ1MzcxOEAxNTUzMzY3MzE4&amp;q=https%3A%2F%2Fgit.kernel.org%2Fpub%2Fscm%2Flinux%2Fkernel%2Fgit%2Ftorvalds%2Flinux.git%2Fcommit%2F%3Fid%3D8a104f8b5867c682d994ffa7a74093c54469c11f&amp;v=y3CbAAwG8dk&amp;event=video_description" target="_blank">https://git.kernel.org/pub/scm/linux/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8gHXQo2gYTlFOvkv6DIBKXdzmz18MTU1MzQ1MzcxOEAxNTUzMzY3MzE4&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FCoraline_Ada_Ehmke&amp;v=y3CbAAwG8dk&amp;event=video_description" target="_blank">https://en.wikipedia.org/wiki/Coralin...
