---
title: "Obscure Window Manager Project - Qtile"
image: images/thumbs/0121.jpg
date: Thu, 22 Feb 2018 21:05:02 +0000
author: Derek Taylor
tags: ["qtile", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+Qtile.mp4" >}}  
&nbsp;

#### SHOW NOTES

Time for window manager number five of the twelve I'm reviewing in the obscure window manager series. This video will cover qtile, a tiling window manager written and configured entirely in Python. Qtile is a very easy-to-use and easy-to-config window manager with great documentation. http://www.qtile.org/
