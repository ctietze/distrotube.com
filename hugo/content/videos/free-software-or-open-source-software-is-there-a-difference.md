---
title: "Free Software or Open Source Software? Is There A Difference?"
image: images/thumbs/0410.jpg
date: Wed, 05 Jun 2019 03:34:48 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Free+Software+or+Open+Source+Software+Is+There+A+Difference.mp4" >}}
&nbsp;

#### SHOW NOTES

"Free software" versus "open source software." Which term is correct? Do they mean the same thing or is there a difference?


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.gnu.org%2Fphilosophy%2Ffree-sw.en.html&amp;redir_token=l3OI5eImS7AvffIedVX7lo63QXl8MTU2NjUzMTI5OUAxNTY2NDQ0ODk5&amp;v=Qyb5KZC7d6s&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.gnu.org/philosophy/free-s...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fopensource.org%2Fosd-annotated&amp;redir_token=l3OI5eImS7AvffIedVX7lo63QXl8MTU2NjUzMTI5OUAxNTY2NDQ0ODk5&amp;v=Qyb5KZC7d6s&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://opensource.org/osd-annotated
