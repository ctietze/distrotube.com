---
title: "A Quick Glance at Kali Linux"
image: images/thumbs/0119.jpg
date: Wed, 21 Feb 2018 20:58:18 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Kali Linux"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-glance-at-kali-linux/55151f694fd8aa5efafa43757dc0e42038f241de?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I take a quick look at the installation procedure for Kali Linux and go through some of the software installed by default. NOTE: I'm not a security, forensics or penetration expert. This is a just cursory glance at Kali.    https://www.kali.org/
