---
title: "PCLinuxOS First Impression Install & Review"
image: images/thumbs/0042.jpg
date: Mon, 27 Nov 2017 14:27:34 +0000
author: Derek Taylor
tags: ["Distro Reviews", "PCLinuxOS", "KDE"]
---

#### VIDEO

{{< amazon src="PCLinuxOS+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at one of the more popular Linux distributions--PCLinuxOS. It is a "stable" rolling release distro that has been around since 2003. I'm installing and reviewing their KDE edition. Oddly, I've never looked at PCLinuxOS before. <a href="http://www.pclinuxos.com/">http://www.pclinuxos.com/</a>
