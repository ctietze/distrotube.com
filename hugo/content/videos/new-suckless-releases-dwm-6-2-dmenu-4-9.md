---
title: "New Suckless Releases - dwm 6.2 and dmenu 4.9"
image: images/thumbs/0347.jpg
date: Tue, 05 Feb 2019 22:47:37 +0000
author: Derek Taylor
tags: ["dwm", "dmenu", "tiling window managers"]
---

#### VIDEO

{{< amazon src="New+Suckless+Releases+dwm+62+and+dmenu+49.mp4" >}}
&nbsp;

#### SHOW NOTES

I just wanted to share with you guys the big news--dwm 6.2 and dmwnu 4.9  were just released.   What's new in these releases?  Good luck finding  that information! 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=wBV0KGTHYyyKx8jJNd4hP5q1FN18MTU1MzY0MDQ2MkAxNTUzNTU0MDYy&amp;v=p-iusifI0rQ&amp;q=https%3A%2F%2Fsuckless.org%2F&amp;event=video_description" target="_blank">https://suckless.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=wBV0KGTHYyyKx8jJNd4hP5q1FN18MTU1MzY0MDQ2MkAxNTUzNTU0MDYy&amp;v=p-iusifI0rQ&amp;q=https%3A%2F%2Flists.suckless.org%2Fdev%2F1902%2F33214.html&amp;event=video_description" target="_blank">https://lists.suckless.org/dev/1902/3...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=wBV0KGTHYyyKx8jJNd4hP5q1FN18MTU1MzY0MDQ2MkAxNTUzNTU0MDYy&amp;v=p-iusifI0rQ&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FXft&amp;event=video_description" target="_blank">https://en.wikipedia.org/wiki/Xft
