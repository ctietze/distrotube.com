---
title: "Nu Shell - A Modern Shell For Today's User"
image: images/thumbs/0445.jpg
date: Sat, 28 Sep 2019 23:51:00 +0000
author: Derek Taylor
tags: ["terminal", "command line", "shell"]
---

#### VIDEO

{{< amazon src="Nu+Shell+Modern+Shell+For+Todays+User.mp4" >}}
&nbsp;

#### SHOW NOTES

I just installed the Nu Shell. Taking it for a quick spin. Looks really interesting. I might make it my default shell for a bit.


#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=l7F9ENtL-FFAchm4WerDm9E7zJV8MTU3NzQ5MDY2OUAxNTc3NDA0MjY5&amp;q=https%3A%2F%2Fgithub.com%2Fnushell%2Fnushell&amp;v=ra1RlD3p1pY&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://github.com/nushell/nushell
