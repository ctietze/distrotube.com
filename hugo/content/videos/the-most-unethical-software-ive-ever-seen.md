---
title: "The Most Unethical Piece Of Software I've Ever Seen"
image: images/thumbs/0634.jpg
date: 2020-05-29T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

{{< amazon src="The+Most+Unethical+Piece+Of+Software+I've+Seen.mp4" >}}
&nbsp;

#### SHOW NOTES

A viewer of the channel recently wrote to me:

"My university 'Western Sydney University' has decided because of the Covid-19, they are going to force students to undertake exams by installing a spyware on their computer (through a browser addon) called ProctorU. This particular program can see what is on your screen, track your entire body including your eyeballs to see if you are cheating or not, check your browser history, collect personal information such as the hardware information of your PC etc. This is a HUGE privacy invasion and it is not right how our university is forcing student to install a spyware on our PCs.

There have been some articles that have criticised how ProctorU can potentially invade people's privacy. I was wondering if you could please do a video on this and discuss how this is a huge serious issue please?"

REFERENCED:
+ https://www.proctoru.com/
+ https://cucfa.org/wp-content/uploads/...
+ https://www.fsf.org/ - Free Software Foundation
+ https://www.eff.org/ - Electronic Frontier Foundation