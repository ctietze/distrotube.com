---
title: "Ubuntu 18.04 Minimal Install Versus Full Install - Dueling VMs"
image: images/thumbs/0136.jpg
date: Wed, 07 Mar 2018 21:25:46 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

{{< amazon src="Ubuntu+18.04+Minimal+Install+Versus+Full+Install+-+Dueling+VMs.mp4" >}}
&nbsp;

#### SHOW NOTES

In this very quick video, I install side-by-side two virtual machines of the Ubuntu 18.04 daily build. On one, I chose the full install. On the other, I chose the minimal install. Then I do a brief comparison on the programs included in each.     EDIT: The minimal install has 1509 packages taking up right at 5G of space. The full has 1550 packages installed taking 5.5G of disk space. 
