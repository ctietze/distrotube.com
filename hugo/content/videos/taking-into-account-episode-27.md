---
title: "Taking Into Account, Ep. 27 - New Pi, Olive, Facebook, 4K Chromebook, App Releases"
image: images/thumbs/0343.jpg
date: Thu, 31 Jan 2019 22:41:48 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+ep+27.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=43s">0:43 Raspberry Pi’s new Compute Module 3+ is ten times faster, but still just as cheap. 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=302s">5:02 Olive is a new open source video editor aiming to take on the pros, like Final Cut Pro. 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=642s">10:42 Facebook’s “research” tool pays users to get complete access to their data. 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=855s">14:15 Now you can buy a Chromebook with a 15.6 inch, 4K display (if that’s something you want to do). 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=1060s">17:40 App news, two new releases: Awesome 4.3 and Flatpak 1.2.  Plus, Gradio is dead. 

<a href="https://www.youtube.com/watch?v=dWk8BQVN1pI&amp;t=1326s">22:06 I read a viewer email. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F01%2Fraspberry-pi-compute-module-3-released" target="_blank">https://www.omgubuntu.co.uk/2019/01/r...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fitsfoss.com%2Folive-video-editor%2F" target="_blank">https://itsfoss.com/olive-video-editor/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=http%3A%2F%2Flibregraphicsworld.org%2Fblog%2Fentry%2Fintroducing-olive-new-non-linear-video-editor" target="_blank">http://libregraphicsworld.org/blog/en...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Ffossbytes.com%2Ffacebooks-research-tool-pays-users-to-get-complete-access-to-their-data%2F" target="_blank">https://fossbytes.com/facebooks-resea...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fliliputing.com%2F2019%2F01%2Fnow-you-can-buy-a-chromebook-with-a-15-6-inch-4k-display-if-thats-something-you-want-to-do.html" target="_blank">https://liliputing.com/2019/01/now-yo...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fawesomewm.org%2Fdoc%2Fapi%2Fdocumentation%2F89-NEWS.md.html" target="_blank">https://awesomewm.org/doc/api/documen...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fblogs.gnome.org%2Fmclasen%2F2019%2F01%2F28%2Fwhats-new-in-flatpak-1-2%2F" target="_blank">https://blogs.gnome.org/mclasen/2019/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=dWk8BQVN1pI&amp;redir_token=E6s-dF1PjgLxCWCYSbKAKTqaAZp8MTU1MzY0MDExM0AxNTUzNTUzNzEz&amp;event=video_description&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F01%2Fgradio-linux-app-replaced-by-shortwave" target="_blank">https://www.omgubuntu.co.uk/2019/01/g...
