---
title: "4Chan At The Command Line"
image: images/thumbs/0511.jpg
date: 2020-01-11T12:22:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/4chan-at-the-command-line/7f980202483fda702f90f9f877dace9b17955c0d?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm sharing with you guys a neat terminal-based 4chan browser called chancli.  It is a Python program licensed under the MIT license.  It has a simple interface that intuitive and easy-to-use.

REFERENCED:
+ Chancli - https://github.com/Gimu/chancli
+ My thread - http://boards.4channel.org/g/thread/74342480
