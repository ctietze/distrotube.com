---
title: "If You Support Free Software, You Should Support Gun Rights" 
image: images/thumbs/0719.jpg
date: 2020-09-21T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

{{< amazon src="If+You+Support+Free+Software%2C+You+Should+Support+Gun+Rights.mp4" >}}
&nbsp;

#### SHOW NOTES

I support the Free Software Movement, and I support the Second Ammendment.  To me, supporting both seems a natural fit.  I see a lot of similarities between the kinds of freedoms that both movements are fighting for.  Yet, I don't see enough of the "free software" crowd here in the US standing up for gun rights. 