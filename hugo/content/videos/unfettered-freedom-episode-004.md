---
title: "Unfettered Freedom, Ep. 4 - Google and FOSS, Patent Trolls, Linux FUD, Firefox, Arch Linux"
image: images/thumbs/0702.jpg
date: 2020-08-26T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+4.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 02:12 - Let's remove Google from FOSS.
+ 10:34 - Courts shouldn't stifle patent troll victim's speech.
+ 15:59 - Enough with the Linux security FUD.
+ 21:30 - Firefox 80.0 has been released.
+ 23:56 - By installing Arch, do you "learn" Linux?
+ 28:55 - Outro and a THANK YOU to the patrons!