---
title: "Setting Up The Mu4e Email Client In Doom Emacs" 
image: images/thumbs/0775.jpg
date: 2020-12-18T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/setting-up-the-mu4e-email-client-in-doom/72fbe067687a313d6e5c6db00883e426d83b62ed?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

Do you use Emacs?  Do you also use a desktop email client like Thunderbird or Geary?  There is no need to run those desktop email clients when Emacs has a great email client available.  That email client is called mu4e.

#### NOTE: 
When I did the git clone of my dotfiles repo and copied over my .doom.d configs, my Doom configs didn't want to work properly initially, because my Doom configs expect the following packages to be installed.  So if you choose to use my Doom configs, make sure to install these!
+  ttf-ubuntu-font-family
+  nerd-fonts-mononoki (AUR)
+  fish

#### REFERENCED:
+  https://github.com/hlissner/doom-emacs - Doom Emacs
+  https://www.djcbsoftware.nl/code/mu/m... - Mu4e
+  https://gitlab.com/dwt1/dotfiles - DT's dotfiles repo