---
title: "ArchLabs Minimo First Impression Install & Review"
image: images/thumbs/0010.jpg
date: Sat, 14 Oct 2017 01:49:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchLabs", "Openbox"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archlabs-minimo-first-impression-install/a3f17d4c35968471c072e67b1978e1b562a1985c?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Yet another Arch-based rolling release distro. This time I take a look at ArchLabs, a Linux distro that I only recently became aware of. A minimal distro that uses the Openbox window manager. https://archlabslinux.com/
