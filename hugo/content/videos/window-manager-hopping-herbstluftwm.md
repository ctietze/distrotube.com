---
title: "Window Manager Hopping: Herbstluftwm"
image: images/thumbs/0368.jpg
date: Sat, 16 Mar 2019 23:17:17 +0000
author: Derek Taylor
tags: ["tiling window managers", "herbstluftwm"]
---

#### VIDEO

{{< amazon src="Window+Manager+Hopping+Herbstluftwm.mp4" >}}
&nbsp;

#### SHOW NOTES

So far in 2019, I've lived in qtile, i3, dwm and xmonad.  It's time to window manager hop once again.  This time--herbstluftwm! 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fherbstluftwm.org%2F&amp;event=video_description&amp;redir_token=RkH-6MFWCY7mfwGG6duRB5ikwVx8MTU1MzY0MjI1M0AxNTUzNTU1ODUz&amp;v=AjJfD2l3tdA" target="_blank">https://herbstluftwm.org/
