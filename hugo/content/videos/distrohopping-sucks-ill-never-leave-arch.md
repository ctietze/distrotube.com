---
title: "Distro Hopping Sucks. I'll Never Leave You Again, Arch Linux!"
image: images/thumbs/0689.jpg
date: 2020-08-08T12:23:40+06:00
author: Derek Taylor
tags: ["Arco Linux", "GNU Guix", "Void Linux"]
---

#### VIDEO

{{< amazon src="Distro+Hopping+Sucks+Ill+Never+Leave+You+Again%2C+Arch+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

In the last 24 hours, I have distrohopped 8 times on my main production machine.  Several failed installs and several bottles of wine later, I realized I messed up.  You never quit a good thing, and I had a good thing with the Arch-based distros, especially Arco.

REFERENCED:
+ https://arcolinux.com/
+ https://guix.gnu.org/
+ https://www.parabola.nu/
+ https://voidlinux.org/
+ https://cloveros.ga/
+ https://www.sabayon.org/
+ https://www.calculate-linux.org/
