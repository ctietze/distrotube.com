---
title: "The Biggest Failure Of Linux Is Package Management"
image: images/thumbs/0694.jpg
date: 2020-08-15T12:23:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="The+Biggest+Failure+Of+Linux+Is+Package+Management.mp4" >}}
&nbsp;

#### SHOW NOTES

I, like many of you, cannot switch away from an Arch-based Linux distribution.  Why?  The AUR!  And while Arch Linux and the AUR are great, the popularity of the AUR highlights one of the biggest weaknesses of the Linux ecosystem.  And that's package management.