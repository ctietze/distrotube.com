---
title: "Newsboat RSS Reader - Not Just For News Feeds"
image: images/thumbs/0357.jpg
date: Sun, 24 Feb 2019 23:03:17 +0000
author: Derek Taylor
tags: ["TUI Apps", "terminal", "newsboat"]
---

#### VIDEO

{{< amazon src="Newsboat+RSS+Reader+Not+Just+For+News+Feeds.mp4" >}}
&nbsp;

#### SHOW NOTES

What kind of RSS feeds can you add to Newboat (or any newsfeed reader,  for that matter) other than your typical news site feeds?  Today, I'm  going to show you a few things that you might consider adding to your  RSS reader. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=CJXdQTGm1jg&amp;redir_token=0a2F3wEKKaDH2tTIcEJFOaG36Gt8MTU1MzY0MTQwM0AxNTUzNTU1MDAz&amp;q=https%3A%2F%2Fnewsboat.org%2F" target="_blank">https://newsboat.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=CJXdQTGm1jg&amp;redir_token=0a2F3wEKKaDH2tTIcEJFOaG36Gt8MTU1MzY0MTQwM0AxNTUzNTU1MDAz&amp;q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles%2Fblob%2Fmaster%2F.newsboat%2Furls" target="_blank">https://gitlab.com/dwt1/dotfiles/blob...
