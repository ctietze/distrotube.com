---
title: "Ubuntu Is No Longer Exciting. They Should Make These Changes." 
image: images/thumbs/0742.jpg
date: 2020-10-26T12:23:40+06:00
author: Derek Taylor
tags: ["Ubuntu", "GNOME"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/ubuntu-is-no-longer-exciting-they-should/0b731b2f0b6254c2320c85802d4d965db36a49fb?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

One thing that is obvious to anyone that has been around Linux for a number of years is that there is not the same level of excitement with the new Ubuntu releases as in the early years.  What happened?  Where did it all go wrong?  And what changes could Ubuntu make to correct this?