---
title: "Namib GNU/Linux 1806 GNOME"
image: images/thumbs/0242.jpg
date: Wed, 11 Jul 2018 00:23:11 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Namib"]
---

#### VIDEO

{{< amazon src="Namib+GNULinux+1806+GNOME.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a quick look at Namib GNU/Linux 1806 GNOME. I gotta say, for a GNOME distro, this thing is pretty sweet! https://www.namiblinux.org/
