---
title: "Getting Started With Awesome Window Manager"
image: images/thumbs/0570.jpg
date: 2020-03-15T12:22:40+06:00
author: Derek Taylor
tags: ["Awesome", " Tiling Window Managers"]
---

#### VIDEO

{{< amazon src="Getting+Started+With+Awesome+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Recently, I asked you guys that have never used a window manager to install one and play around with it.  I suggested the awesome window manager. Some of you did install awesome and are now asking, "What do I do now?"  Here's a video on getting  started with the awesome window manager.

REFERENCED:
+ https://awesomewm.org/ - Official Awesome Website