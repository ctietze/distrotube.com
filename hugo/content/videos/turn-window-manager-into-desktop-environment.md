---
title: "Turn Your Window Manager Into A Desktop Environment" 
image: images/thumbs/0776.jpg
date: 2020-12-19T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/turn-your-window-manager-into-a-desktop/fcd5e3f6000d2ada27ff89eef1de1446c4bfe220?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

So you installed a stand-alone window manager, but a window manager is not a desktop environment.  It's just window manager.  It manages windows.  It DOES NOT manage notifications, clipboards, volume controls, bluetooth, etc.  You have to install programs to do those tasks.  

REFERENCED:
+ https://wiki.archlinux.org/ - Arch Wiki