---
title: "Xmonad Is The Super Hackable Tiling Window Manager"
image: images/thumbs/0644.jpg
date: 2020-06-12T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Xmonad+Is+The+Super+Hackable+Tiling+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Xmonad has been my favorite tiling window manager for many years now, and yet I'm still amazed at some of what it Xmonad can do.  In this video, I will discuss some Xmonad modules that allow you to do some very cool stuff, including submapping your keybindings and having custom search prompts.

REFERENCED:
+ https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Util-EZConfig.html
+ https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-Submap.html
+ https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-Search.html