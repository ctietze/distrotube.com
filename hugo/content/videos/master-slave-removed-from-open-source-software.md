---
title: "Master and Slave Are Being Removed From Open Source Software"
image: images/thumbs/0653.jpg
date: 2020-06-23T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Master+and+Slave+Are+Being+Removed+From+Open+Source+Software.mp4" >}}
&nbsp;

#### SHOW NOTES

Due to the political climate, many organizations and open source projects are removing the terms "master" and/or "slave" from their software.  GitHub is changing "master" branches to "main" branches.  Master-slave terminology in regards to drives, file systems, programming languages, etc. are being removed as well.  This has caused some debate in the community about whether these changes are good or not.  Here's my brief thoughts on the matter.