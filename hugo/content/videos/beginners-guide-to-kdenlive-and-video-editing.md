---
title: "Beginner's Guide To Kdenlive And Video Editing"
image: images/thumbs/0684.jpg
date: 2020-08-01T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "Linux Video"]
---

#### VIDEO

{{< amazon src="Beginner's+Guide+To+Kdenlive+And+Video+Editing.mp4" >}}
&nbsp;

#### SHOW NOTES

Kdenlive is the premiere open source video editor.  It is the video editor of choice, not just for me, but for many video content creators on platforms like YouTube and LBRY.  It is free as in freedom and free as in cost.  Kdenlive is jam-packed with features, which makes it a bit challenging to learn. 

REFERENCED:
+ https://kdenlive.org/en/ - Kdenlive
+ https://kde.org/donations - Donate to the KDE team