---
title: "Hannah Montana Linux Installation and First Look"
image: images/thumbs/0217.jpg
date: Fri, 25 May 2018 23:37:54 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Hannah Montana Linux"]
---

#### VIDEO

{{< amazon src="Hannah+Montana+Linux+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at one of the most legendary Linux distributions ever made--Hannah Montana Linux! http://hannahmontana.sourceforge.net/
