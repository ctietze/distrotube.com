---
title: "A Few Vim Plugins That Have Impressed Me Recently"
image: images/thumbs/0583.jpg
date: 2020-03-30T12:22:40+06:00
author: Derek Taylor
tags: ["Vim", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-few-vim-plugins-that-have-impressed-me/a05cc30be9def900ed6c949b08d69de30032189d?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

So I've moved back to Vim from Emacs, and having spent a few days back in Vim, I've been looking at a few different plugins.  I thought I would share a few plugins that I think are worthy of consideration.

REFERENCED:
+ https://github.com/itchyny/lightline.vim - lightline.vim
+ https://github.com/mcchrish/nnn.vim - nnn.vim
+ https://github.com/vifm/vifm.vim - vifm.vim
+ https://github.com/vimwiki/vimwiki - vimwiki
+ https://github.com/ap/vim-css-color - vim-css-color