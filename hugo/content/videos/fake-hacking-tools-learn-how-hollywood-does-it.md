---
title: "Fake Hacking Tools - Learn How Hollywood Does It"
image: images/thumbs/0257.jpg
date: Fri, 17 Aug 2018 22:38:00 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Fake+Hacking+Tools+-+Learn+How+Hollywood+Does+It.mp4" >}}
&nbsp;

#### SHOW NOTES

How you can become a "leet" hacker by using these simple tools and tricks. This is how Hollywood does it! 
+ https://hackertyper.net/ 
+ http://geektyper.com/ 
+ https://geekprank.com/hacker/ 
+ http://fakeupdate.net/ 
+ https://github.com/abishekvashok/cmatrix 
+ https://github.com/bartobri/no-more-secrets
