---
title: "A Linux Shell Prompt That's Out Of This World!" 
image: images/thumbs/0768.jpg
date: 2020-12-05T12:23:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-linux-shell-prompt-that-s-out-of-this/a7a843190ab21ee88e7baff928d2193a31c7e58f?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Starship is the minimal, blazing-fast, and infinitely customizable prompt for any shell!  It is written in Rust and is compatible with bash, fish and zsh.  It has a ton of features, and the onfiguration is super easy.

REFERENCED:
+ https://starship.rs/