---
title: "Chat With Patrons (September 13, 2020)" 
image: images/thumbs/0712.jpg
date: 2020-09-13T12:23:40+06:00
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(September+13%2C+2020)-SdqwB2KMptU.mp4" >}}
&nbsp;

#### SHOW NOTES

This Video Chat will be for my Patrons!  Patrons can join the video call via Jitsi which is available on Linux, Mac and Windows.  For those wishing to join the chat but are not a Patron, consider joining my Patreon ( https://www.patreon.com/distrotube ).  I will post a link to the Jitsi chat on my Patreon page a few minutes prior to the stream.  Hope to see you guys there!