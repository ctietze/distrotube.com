---
title: "Taking Into Account, Ep. 29 - Linux gaming, Taking Code Back, Microsoft OpenChain, Snapd, Void Linux"
image: images/thumbs/0351.jpg
date: Thu, 14 Feb 2019 22:54:28 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+29.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=42s">0:42  After six months with Linux, one writer shares the ugly truth about gaming without Windows. 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=667s">11:07 No, you can't take open source code back!  Let's clear up this open source myth once and for all. 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=1120s">18:40 Microsoft continues efforts to support open source.  Joins  OpenChain platform. 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=1366s">22:46 Canonical snapd vulnerability, Dirty_Sock, gives root access in  Linux. 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=1608s">26:48 Void Linux wants you to know voidlinux.eu, their former domain, is no longer theirs. 

<a href="https://www.youtube.com/watch?v=pST8A1hPdZM&amp;t=1968s">32:48 I read a viewer question from Mastodon. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F02%2F12%2F6-months-with-linux-the-ugly-truth-about-gaming-without-windows%2F&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fno-you-cant-take-open-source-code-back%2F&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://www.zdnet.com/article/no-you-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.techradar.com%2Fnews%2Fmicrosoft-joins-openchain-platform&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://www.techradar.com/news/micros...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fcloudblogs.microsoft.com%2Fopensource%2F2019%2F02%2F06%2Fmicrosoft-joins-openchain-community%2F&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://cloudblogs.microsoft.com/open...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.bleepingcomputer.com%2Fnews%2Fsecurity%2Fcanonical-snapd-vulnerability-gives-root-access-in-linux%2F&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://www.bleepingcomputer.com/news...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2Faq29ap%2Fvoidlinuxeu_is_not_ours%2F&amp;v=pST8A1hPdZM&amp;redir_token=Pdw6FKxogMuA9_BjCMjcOf-gDQZ8MTU1MzY0MDg5MUAxNTUzNTU0NDkx&amp;event=video_description" target="_blank">https://www.reddit.com/r/linux/commen...
