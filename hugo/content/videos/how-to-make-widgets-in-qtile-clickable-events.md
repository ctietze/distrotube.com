---
title: "How To Make Widgets In Qtile Clickable Events"
image: images/thumbs/0660.jpg
date: 2020-07-04T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "qtile"]
---

#### VIDEO

{{< amazon src="How+To+Make+Widgets+In+Qtile+Clickable+Events.mp4" >}}
&nbsp;

#### SHOW NOTES

I recently discovered that you can make any widget in the Qtile panel a clickable event using mouse callbacks.  This opens up a world of possibilities!  So I go through the process of adding some of these mouse callbacks to my config.

REFERENCED:
+ http://www.qtile.org/