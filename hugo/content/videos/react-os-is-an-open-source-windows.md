---
title: "ReactOS Is An Open Source Windows-Inspired Operating System"
image: images/thumbs/0595.jpg
date: 2020-04-12T12:23:40+06:00
author: Derek Taylor
tags: ["ReactOS", "Windows"]
---

#### VIDEO

{{< amazon src="ReactOS+Is+An+Open+Source+Windows-Like+Operating+System.mp4" >}}
&nbsp;

#### SHOW NOTES

Imagine running your favorite Windows applications and drivers in an open-source environment you can trust. That's the mission of ReactOS!  ReactOS has been under development since 2004 and is still considered alpha software.

REFERENCED:
+ https://reactos.org/