---
title: "View Gopherspace In 3D With GopherVR"
image: images/thumbs/0645.jpg
date: 2020-06-13T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="View+Gopherspace+In+3D+With+GopherVR.mp4" >}}
&nbsp;

#### SHOW NOTES

Gopher doesn't have to be just plain text in a terminal.  You can view Gopherspace in 3D environment with GopherVR.  This stunning, cutting-edge technology (from the mid 90s) is available on Mac, Linux and BSD.

REFERENCED:
+ https://en.wikipedia.org/wiki/GopherVR