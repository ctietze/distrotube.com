---
title: "Rofi - Application Launcher, Window Switcher and Run Command Utility"
image: images/thumbs/0066.jpg
date: Mon, 18 Dec 2017 15:25:13 +0000
author: Derek Taylor
tags: ["Command Launcher", "rofi"]
---

#### VIDEO

{{< amazon src="Rofi+-+Application+Launcher%2C+Window+Switcher+and+Run+Command+Utility.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at a cool, little app launcher named Rofi. It is simple-to-use and highly configurable. A perfect launcher program or run command utility for minimal desktop environments or window managers. <a href="https://github.com/DaveDavenport/rofi/">https://github.com/DaveDavenport/rofi/</a>
