---
title: "Movie Monad - Lightweight And Cross Platform Video Player"
image: images/thumbs/0354.jpg
date: Tue, 19 Feb 2019 22:58:50 +0000
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Movie+Monad+Lightweight+And+Cross+Platform+Video+Player.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you looking for a lightweight and cross platform video player?   Movie Monad might just be the player that you have been looking for!   It's written in Haskell and available in a number of different package  formats, including AppImage, Flatpak and Snap. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Flettier%2Fmovie-monad&amp;redir_token=Oc2lVdOKWiCpX2UnW74J9baPe5J8MTU1MzY0MTE0MUAxNTUzNTU0NzQx&amp;v=NT7IPGA3eXk&amp;event=video_description" target="_blank">https://github.com/lettier/movie-monad
