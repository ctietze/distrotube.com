---
title: "Made my decision. Installing AntiX 17 on my production machine."
image: images/thumbs/0148.jpg
date: Sun, 18 Mar 2018 21:41:29 +0000
author: Derek Taylor
tags: ["AntiX", ""]
---

#### VIDEO

{{< amazon src="Made+my+decision.+Installing+AntiX+17+on+my+production+machine..mp4" >}}  
&nbsp;

#### SHOW NOTES

Just a quick video announcing my decision to install AntiX on my main production machine. Been running Manjaro on this machine for three months with no issues. Time to move on to something else for a bit. <a href="https://antixlinux.com/">https://antixlinux.com/</a>
