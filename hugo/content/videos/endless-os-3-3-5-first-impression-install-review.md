---
title: "Endless OS 3.3.5 First Impression Install & Review"
image: images/thumbs/0069.jpg
date: Tue, 19 Dec 2017 15:48:45 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Endless OS"]
---

#### VIDEO

{{< amazon src="Endless+OS+3.3.5+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at this Debian-based distro that sports their own custom desktop that is a fork of GNOME 3--it is their own custom GNOME shell called the EOS Shell. Endless OS is geared toward education and is loaded down with it's own custom apps that deliver over 50,000 articles to the end user. <a href="https://endlessos.com">https://endlessos.com</a>
