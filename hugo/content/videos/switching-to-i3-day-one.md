---
title: "Switching to i3 - Day One"
image: images/thumbs/0326.jpg
date: Tue, 01 Jan 2019 19:49:41 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm"]
---

#### VIDEO

{{< amazon src="Switching+to+i3+-+Day+One.mp4" >}}
&nbsp;

#### SHOW NOTES

Happy New Year.   Today is January 1, 2019.   A New Year deserves a new window manager.  I asked you guys to help decide what window manager I should use in 2019:  <a href="https://www.youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg/community">https://www.youtube.com/channel/UCVls... Overwhelmingly, you guys wanted me to use i3.   So here I am...DAY ONE!
