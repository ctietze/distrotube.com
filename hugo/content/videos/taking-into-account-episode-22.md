---
title: "Taking Into Account, Ep. 22 - Lubuntu 32-bit, The Fake Internet, FSF, Distros in 2018, Polo"
image: images/thumbs/0315.jpg
date: Thu, 27 Dec 2018 19:28:11 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=43s">0:43 Lubuntu kicks 32-bit users to the curb.  Drops 32-bit images. 

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=517s">8:37 How much of the Internet is fake?  A lot, actually!  

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=1007s">16:47 Reviewing 2018 in regards to software freedom. 

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=1420s">23:40 Looking back at the 5 best distros released in 2018. 

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=1791s">29:51 The most advanced file manager ever?  Check out Polo if you don't mind paying for it. 

<a href="https://www.youtube.com/watch?v=9Aom5b_nUe4&amp;t=2134s">35:34 I read a viewer question from Mastodon. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fbetanews.com%2F2018%2F12%2F21%2Flubuntu-linux-32bit%2F&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">https://betanews.com/2018/12/21/lubun...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Flubuntu.me%2Fsunsetting-i386%2F&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">https://lubuntu.me/sunsetting-i386/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=http%3A%2F%2Fnymag.com%2Fintelligencer%2F2018%2F12%2Fhow-much-of-the-internet-is-fake.html&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">http://nymag.com/intelligencer/2018/1...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.fsf.org%2Fblogs%2Fcommunity%2Fsmall-victories-matter-the-year-in-free-software&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">https://www.fsf.org/blogs/community/s...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2018%2F12%2Fbest-linux-distros-2018&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">https://www.omgubuntu.co.uk/2018/12/b...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fitsfoss.com%2Fpolo-file-manager%2F&amp;redir_token=5JiVMrnZPFl_1tI83xOJgwf2ta58MTU1MzU0MjA4MEAxNTUzNDU1Njgw&amp;event=video_description&amp;v=9Aom5b_nUe4" target="_blank">https://itsfoss.com/polo-file-manager/
