---
title: "The Web Is Broken Beyond Repair. The Alternative? GOPHER!"
image: images/thumbs/0381.jpg
date: Fri, 12 Apr 2019 02:51:43 +0000
author: Derek Taylor
tags: ["gopher", ""]
---

#### VIDEO

{{< amazon src="The+Web+Is+Broken+Beyond+Repair+The+Alternative+GOPHER.mp4" >}}
&nbsp;

#### SHOW NOTES

Gopher is an alternative protocol to the standard http (world wide web)  protocol. The modern web is slow, bloated and just sucks due to the  Internet being dominated by the likes of Google, Facebook, Microsoft and  others. Websites are now built with CSS, JavaScript, PHP, databases,  images, multimedia and, of course, advertisements!  It's not unusual to  open Chrome of Firefox and push over 1GB of RAM usage with just one tab  open. This is absurd! 

📰 REFERENCED: 

gopher://distro.tube 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=lUBhOgK5zQI&amp;event=video_description&amp;redir_token=vavdx4DLlQAEkbFnD5U88xQYQy58MTU1NzE5NzQ5NUAxNTU3MTExMDk1&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FGopher_%28protocol" target="_blank">https://en.wikipedia.org/wiki/Gopher_...) 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=lUBhOgK5zQI&amp;event=video_description&amp;redir_token=vavdx4DLlQAEkbFnD5U88xQYQy58MTU1NzE5NzQ5NUAxNTU3MTExMDk1&amp;q=https%3A%2F%2Fgithub.com%2Fjgoerzen%2Fpygopherd" target="_blank">https://github.com/jgoerzen/pygopherd
