---
title: "Fedora 30 Workstation - Installation And First Impression"
image: images/thumbs/0385.jpg
date: Tue, 30 Apr 2019 02:58:32 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Fedora"]
---

#### VIDEO

{{< amazon src="Fedora+30+Workstation+Installation+And+First+Impression.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at the newly released Fedora 30 Workstation with the GNOME 3.32 desktop.   

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=0OR47yr89JQ&amp;q=https%3A%2F%2Ffedoramagazine.org%2Fannouncing-fedora-30%2F&amp;redir_token=nTwhSY0TBnvP1JHyb-onCwL0kAN8MTU1NzE5ODA4NEAxNTU3MTExNjg0" target="_blank">https://fedoramagazine.org/announcing...
