---
title: "Taking Into Account, Ep 44 - Angry Devs, Quake, Snap Store, Linux Mint Should Die"
image: images/thumbs/0406.jpg
date: Thu, 30 May 2019 03:25:24 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+44.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=55s">0:55 App devs ask Linux distros to "stop theming our apps." Epiphany devs complain about Ubuntu packaging.
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=605s">10:05 Quake II RTX is coming to Windows and Linux and you can have it for free.
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=789s">13:09 The state of Linux graphic design tools in 2019. Can open source handle professional work?
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=1103s">18:23 Did you know that Canonical’s Snap store is available to install as a Snap app?
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=1343s">22:23 Scientific Linux and Antergos are shutting down: It's time for Linux Mint to go.
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=FOqss0y7v-A&amp;t=2008s">33:28 I read a viewer question from Mastodon.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F05%2Fopen-letter-stop-gtk-theming-distros&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://www.omgubuntu.co.uk/2019/05/o...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fblogs.gnome.org%2Fmcatanzaro%2F2019%2F05%2F24%2Fdear-ubuntu-please-stop-packaging-epiphany-if-you-wont-do-it-properly%2F&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://blogs.gnome.org/mcatanzaro/20...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fbetanews.com%2F2019%2F05%2F29%2Fquake-ii-rtx-windows-linux-free%2F&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://betanews.com/2019/05/29/quake...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fopensource.com%2Farticle%2F19%2F4%2Flinux-graphic-design-tools-professionals&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://opensource.com/article/19/4/l...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F05%2Fsnapception-the-snap-store-is-now-available-as-a-snap-app&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://www.omgubuntu.co.uk/2019/05/s...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=FOqss0y7v-A&amp;q=https%3A%2F%2Fwww.techrepublic.com%2Farticle%2Fscientific-linux-and-antergos-are-shutting-down-its-time-for-linux-mint-to-go%2F&amp;redir_token=SIw2HhzED4X61bkPY08BpRquS5R8MTU2NjUzMDgzOUAxNTY2NDQ0NDM5" target="_blank" rel="nofollow noopener noreferrer">https://www.techrepublic.com/article/...
