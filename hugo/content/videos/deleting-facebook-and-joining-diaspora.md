---
title: "Deleting Facebook and Joining Diaspora"
image: images/thumbs/0196.jpg
date: Fri, 04 May 2018 22:58:22 +0000
author: Derek Taylor
tags: ["Social Media", ""]
---

#### VIDEO

{{< amazon src="Deleting+Facebook+and+Joining+Diaspora.mp4" >}}
&nbsp;

#### SHOW NOTES

Facebook does not respect the privacy of its users. I could never recommend the viewers of this channel to use such a service, therefore I've requested Facebook to delete my account and the all the pages associated with it (including the DistroTube page). I've signed up on Diaspora. Don't know much about Diaspora...yet. But if you guys want to join me... https://diasporafoundation.org/
