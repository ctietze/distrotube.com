---
title: "Redcore Linux 1801 Install & Review - Improving With Each Release"
image: images/thumbs/0102.jpg
date: Tue, 30 Jan 2018 01:33:03 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Redcore Linux"]
---

#### VIDEO

{{< amazon src="Redcore+Linux+1801+Install+%26+Review+-+Improving+With+Each+Release.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I take a look at the latest release of Redcore Linux 1801. It is a Gentoo-based distro that uses the LXQt desktop environment. Notable features for this release include: patches for Meltdown and Spectre, the removal of needing a login/pass to launch the installer, and improved theming and artwork.     I want to commend Ghiunhan Mamut (aka V3n3RiX) on a job well done. He took a couple of my suggestions/criticisms from my earlier review and took action on them. Can't wait to see where Redcore goes in the future. <a href="https://redcorelinux.org/">https://redcorelinux.org/</a>
