---
title: "What Is 'Software Bloat'?"
image: images/thumbs/0393.jpg
date: Tue, 14 May 2019 14:32:06 +0000
author: Derek Taylor
tags: ["bloat", ""]
---

#### VIDEO

{{< amazon src="What+Is+Software+Bloat.mp4" >}}
&nbsp;

#### SHOW NOTES

What is bloat? People often label this software or that software as "bloated." What are they talking about? And should we be concerned?
