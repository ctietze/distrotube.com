---
title: "Redcore Linux First Impression Install & Review"
image: images/thumbs/0031.jpg
date: Wed, 08 Nov 2017 14:06:03 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Redcore", "LXQt"]
---

#### VIDEO

{{< amazon src="Redcore+Linux+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I'm installing a Gentoo-based distro named Redcore Linux. It is a rolling release Linux distro that aims to provide a Gentoo-like experience but with a quick and user-friendly installation.
