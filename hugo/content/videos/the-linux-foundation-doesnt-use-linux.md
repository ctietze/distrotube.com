---
title: "The Linux Foundation Doesn't Use Linux To Create Their Reports" 
image: images/thumbs/0705.jpg
date: 2020-09-03T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

{{< amazon src="The+Linux+Foundation+Doesn't+Use+Linux+To+Create+Their+Reports.mp4" >}}
&nbsp;

#### SHOW NOTES

The Linux Foundation was spotted, once again, not using the Linux operating system.  In a recent report that they published, you can see that it was created on a Mac using Adobe software.  Here are my thoughts on video and in the email below:

MY EMAIL TO THE LINUX FOUNDATION:
info@linuxfoundation.org
To Whom It May Concern:

I am long-time Linux user and supporter of free and open source software, but I am growing increasingly concerned that The Linux Foundation and its employees may not share my values regarding Linux and open source.  I noticed that the recently published 2020 Linux Kernel History Report was written on a Mac using Adobe software.  I find it strange that anyone associated with The "Linux" Foundation would be using a Mac.  And considering that your website states that your organization is committed to providing support for the open source community, I find it even more bizarre that proprietary garbage, such as the software produced by Adobe, is being used to create basic PDF presentations.

Personally, I love Linux and open source.  Linux has influenced my life in a positive way, and I want Linux to continue to grow and awareness of Linux to spread.  But for this to happen, I think that The Linux Foundation and its employees should play a pivotal role in this growth.  And I don't see this happening.  I don't think it is crazy to imagine a time when the open source community, which tends to be rather idealistic, becomes so uncomfortable with the directions of the Foundation that many in the community abandon Linux altogether.  After all, if The Linux Foundation cares so little about Linux and open source, why should I, as an open source advocate, care about Linux?

Sincerely,
Derek Taylor