---
title: "0 A.D Is A Superb Free (As In Freedom) Game - DT LIVE" 
image: images/thumbs/0772.jpg
date: 2020-12-12T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Linux Gaming"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/0-a-d-is-a-superb-free-as-in-freedom/f905f4b535d8cbac30bef505790a0dae4b39eeca?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

0 A.D is a free and open source real-time strategy game, similar in spirit to Age of Empires.  I am not much of a gamer, and 0 A.D is not something that I often win at, but I do enjoy playing this game.  And I love promoting open source games, especially those that are native to Linux.   

REFERENCED:
+ https://play0ad.com/ - 0 A.D