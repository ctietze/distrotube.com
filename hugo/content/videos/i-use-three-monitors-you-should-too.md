---
title: "I Use Three Monitors (And You Should Too!)" 
image: images/thumbs/0778.jpg
date: 2020-12-22T12:23:40+06:00
author: Derek Taylor
tags: ["Productivity", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/i-use-three-monitors-and-you-should-too/04e23869ae868912cf68a460422b6bdc6c6d2dd5?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

Every computer wants to increase their efficiency and productivity.  Better hardware will certainly help in this regard.  But before you drop massive amounts of money on better CPUs and more RAM, have you thought about investing in multiple monitors?