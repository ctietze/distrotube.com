---
title: "My YouTube Studio Tour" 
image: images/thumbs/0729.jpg
date: 2020-10-08T12:23:40+06:00
author: Derek Taylor
tags: ["YouTube", "Audio"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/my-youtube-studio-tour/91a8f118767d15d425b33c0dc1aa519daa998856?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Viewers of the channel have been wanting a rundown of the equipment that I use.  So here is a brief tour of my "studio".  Below, I will post some affiliate links to most of the equipment.

#### REFERENCED:
+ https://amzn.to/3iJ2v67 - Electro-Voice 27 N/D microphone
+ https://amzn.to/3jIT87H - Mount for EV 27 N/D
+ https://amzn.to/30PPpOy - Rode PSA1 Swivel Arm
+ https://amzn.to/3jJyHI1 - Panasonic Lumix G7 Mirrorless Camera
+ https://amzn.to/3nEoOhh - 25mm Lens for Panasonic G7
+ https://amzn.to/34GR2z9 - Audio Technica ATH1000X headphones
+ https://amzn.to/2GLEiPw - Audio Technica M40x headphones
+ https://amzn.to/2IcsRRB - Raising Electronics Server Rack
+ https://amzn.to/3lq0AoT - Penn Elcom 1U LED Light
+ https://amzn.to/2SE3FVX - Mackie ProFX12 Mixer
+ https://amzn.to/3nx9EKw - Mounting bracket for ProFX12
+ https://amzn.to/36LxrQX - Behringer Sonic Exciter
+ https://amzn.to/34HuNJe - Dbx 231s dual-channel EQ
+ https://amzn.to/3lq1GRx - Dbx 166xs compressor, limiter, gate
+ https://amzn.to/3daPGQY - Fovitec Portable LED Lights
+ https://amzn.to/36RRRId - Zesta Saltine Crackers