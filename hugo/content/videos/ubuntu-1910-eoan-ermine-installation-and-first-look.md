---
title: "Ubuntu 19.10 'Eoan Ermine' - Installation and First Look"
image: images/thumbs/0449.jpg
date: Thu, 17 Oct 2019 23:57:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

{{< amazon src="Ubuntu+1910+Eoan+Ermine+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

It's Ubuntu Release Day! So I'm taking a look at the newly released Ubuntu 19.10, code named "Eoan Ermine."

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=a2NjG2weogQ&amp;q=https%3A%2F%2Fubuntu.com%2F&amp;redir_token=6HRl7dMCAqbQLwSzgNaKdFsatPh8MTU3NzQ5MTA5OUAxNTc3NDA0Njk5" target="_blank" rel="nofollow noopener noreferrer">https://ubuntu.com/
