---
title: "Combine The Best of Bspwm and XMonad, You Get Herbstluftwm!"
image: images/thumbs/0676.jpg
date: 2020-07-24T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "herbstluftwm"]
---

#### VIDEO

{{< amazon src="Combine+The+Best+of+Bspwm+and+XMonad%2C+You+Get+Herbstluftwm.mp4" >}}
&nbsp;

#### SHOW NOTES

Herbstluftwm is a manual tiling window manager that has a unique feature set.  The layouts are based on splitting frames into subframes similar to other manual tilers like i3 and bspwm.  It's workspaces (or tags) are monitor independent which is similar to tilers like xmonad and qtile.  It's configuration is easy and the config file can be written in any language that you prefer.

REFERENCED:
+ https://herbstluftwm.org/