---
title: "Parrot Security and Parrot Studio 4.0.1 First Look"
image: images/thumbs/0220.jpg
date: Tue, 29 May 2018 23:42:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Parrot"]
---

#### VIDEO

{{< amazon src="Parrot+Security+and+Parrot+Studio+4.0.1+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick first look at the just released Parrot Security 4.0.1 and the Parrot Studio 4.0.1. http://parrotsec.org/
