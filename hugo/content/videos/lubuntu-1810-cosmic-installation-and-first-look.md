---
title: "Lubuntu 18.10 'Cosmic Cuttlefish' Installation and First Look"
image: images/thumbs/0307.jpg
date: Wed, 14 Nov 2018 00:26:51 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Lubuntu"]
---

#### VIDEO

{{< amazon src="Lubuntu+1810+Cosmic+Cuttlefish+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm finally taking a look at Lubuntu 18.10 with the LXQt desktop environment.  

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Flubuntu.me%2F&amp;v=4POtdFs1meo&amp;event=video_description&amp;redir_token=7oCyuJaacVmLAXW3ejp0q__oFTB8MTU1MzQ3MzYzOUAxNTUzMzg3MjM5" rel="noreferrer noopener" target="_blank">https://lubuntu.me/
