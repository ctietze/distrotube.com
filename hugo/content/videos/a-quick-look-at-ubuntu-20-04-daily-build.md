---
title: "A Quick Look At Ubuntu 20.04 Daily Build"
image: images/thumbs/0562.jpg
date: 2020-03-05T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-ubuntu-20-04-daily-build/b5a03c6ea9351a55b659ef927c0b5204d5274e85?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Another edition of DT Live!  I will take a look at the daily build of UUbuntu 20.04 Focal Fossa.   May discuss some other Linux-y topics.  And I will converse with you guys hanging out in the YouTube chat!   Be there or be square.

REFERENCED:
+ http://cdimage.ubuntu.com/daily-live/pending/
