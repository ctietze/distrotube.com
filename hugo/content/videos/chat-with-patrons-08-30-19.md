---
title: "Chat With Patrons (August 30, 2019)"
image: images/thumbs/0505.jpg
date: Fri, 30 Aug 2019 19:02:44 +0000
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(August+30+2019)-itMve82IsIU.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be a special event for my Patreon supporters. This will be a Zoom video chat just for my patrons. The link to the video chat call will be posted on my Patreon page a few minutes prior to the stream.
