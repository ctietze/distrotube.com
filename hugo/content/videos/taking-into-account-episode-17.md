---
title: "Taking Into Account, Ep. 17 - Ubuntu, Unikernels, Patents, Unity Shell, Thanksgiving"
image: images/thumbs/0310.jpg
date: Thu, 22 Nov 2018 00:33:52 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+17.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=38s">0:38 Ubuntu is extending support from 5 years to 10 years on 18.04 LTS. 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=260s">4:20 Unikernels are coming, offering advantages in performance and security. 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=454s">7:34 Software patents are harmful to innovation, to the Internet and to the human race.  Here's why. 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=698s">11:38 Unity Shell is a GNOME extension that tweaks the desktop to look like Unity. 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=870s">14:30 This Thanksgiving, give thanks to free and open source software maintainers. 10 ways to give. 

<a href="https://www.youtube.com/watch?v=KM0_PWX00es&amp;t=1177s">19:37 I read a viewer question from Mastodon. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8g8nq9G7AMnEHeRqOSOU8zl3Kgd8MTU1MzQ3NDAyMEAxNTUzMzg3NjIw&amp;q=https%3A%2F%2Fitsfoss.com%2Fubuntu-18-04-ten-year-support%2F&amp;v=KM0_PWX00es&amp;event=video_description" target="_blank">https://itsfoss.com/ubuntu-18-04-ten-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8g8nq9G7AMnEHeRqOSOU8zl3Kgd8MTU1MzQ3NDAyMEAxNTUzMzg3NjIw&amp;q=https%3A%2F%2Fhackaday.com%2F2018%2F11%2F18%2Flinux-as-a-library-unikernels-are-coming%2F&amp;v=KM0_PWX00es&amp;event=video_description" target="_blank">https://hackaday.com/2018/11/18/linux...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8g8nq9G7AMnEHeRqOSOU8zl3Kgd8MTU1MzQ3NDAyMEAxNTUzMzg3NjIw&amp;q=https%3A%2F%2Fmedium.com%2F%40jdrosen2%2Fsoftware-patents-considered-harmful-868166fa437d&amp;v=KM0_PWX00es&amp;event=video_description" target="_blank">https://medium.com/@jdrosen2/software...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8g8nq9G7AMnEHeRqOSOU8zl3Kgd8MTU1MzQ3NDAyMEAxNTUzMzg3NjIw&amp;q=https%3A%2F%2Fgithub.com%2Fhardpixel%2Funite-shell&amp;v=KM0_PWX00es&amp;event=video_description" target="_blank">https://github.com/hardpixel/unite-shell

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=8g8nq9G7AMnEHeRqOSOU8zl3Kgd8MTU1MzQ3NDAyMEAxNTUzMzg3NjIw&amp;q=https%3A%2F%2Fopensource.com%2Farticle%2F18%2F11%2Fways-give-thanks-open-source&amp;v=KM0_PWX00es&amp;event=video_description" target="_blank">https://opensource.com/article/18/11/...
