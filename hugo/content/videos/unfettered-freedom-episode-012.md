---
title: "Unfettered Freedom Ep. 12 - Linus on M1 Mac, Snaps 2020, Funtoo, Sabayon, Fedora Pipewire, Systemd" 
image: images/thumbs/0762.jpg
date: 2020-11-25T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/unfettered-freedom-ep-12-linus-on-m1-mac/baf8afb0648b825a53fe491e515decd07072ac0c?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:
+ 0:00 Intro
+ 1:37 Linus Torvalds wants Linux to run on M1 Mac
+ 8:00 The most popular Snap packages in 2020
+ 14:15 Funtoo and Sabayon are joining forces
+ 18:44 Fedora switching from Pulse/Jack with Pipewire?
+ 23:55 Lennart Poettering wants systemd to have more control of home directory
+ 28:29 Outro and THANK YOU to the Patrons!

