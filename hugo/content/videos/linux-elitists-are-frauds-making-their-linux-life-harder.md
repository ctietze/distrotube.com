---
title: "Linux Elitists Are Frauds - Making Their Linux Life Harder To Look Cool"
image: images/thumbs/0450.jpg
date: Fri, 25 Oct 2019 23:59:00 +0000
author: Derek Taylor
tags: ["command line", "tiling window managers", "Vim", "Emacs"]
---

#### VIDEO

{{< amazon src="Linux+Elitists+Are+Frauds+Making+Their+Linux+Life+Harder+To+Look+Cool.mp4" >}}
&nbsp;

#### SHOW NOTES

Are Linux elitists a detriment to the Linux community? Are they frauds who only use things like tiling window managers and CLI apps to show off and to rub it in the faces of the Linux noobs?
