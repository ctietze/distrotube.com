---
title: "Taking Into Account, Ep. 47 - Endeavour, Alternative OSes, VLC Flaw, Dropbox, Deepin, Me and GNOME"
image: images/thumbs/0424.jpg
date: Thu, 25 Jul 2019 05:44:00 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+47.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=54s">0:54 New Arch Linux-Based Endeavour OS Launches To Keep Spirit Of Antergos Alive
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=445s">7:25 Forget Windows, Linux or MacOS: Try these alternative operating systems
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=742s">12:22 Critical flaw in VLC Player affects Linux, Windows and UNIX apps
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=875s">14:35 Dropbox added support for zfs, eCryptFS, xfs, and btrfs filesystems in Linux
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=1035s">17:15 The New Version Of Deepin Linux Has A Killer Feature That Every Distribution Needs
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=Rm13QRauRb0&amp;t=1300s">21:40 I read viewer comment from Mastodon.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F07%2F16%2Fnew-arch-based-endeavour-os-launches-to-keep-spirit-of-antergos-linux-alive%2F%232f4b4cf33507&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.forbes.com/sites/jasoneva...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.techradar.com%2Fnews%2Fbest-alternative-operating-systems&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.techradar.com/news/best-a...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.theinquirer.net%2Finquirer%2Fnews%2F3079345%2Fvlc-critical-flaw-linux-windows-unix&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.theinquirer.net/inquirer/...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.dropboxforum.com%2Ft5%2FDesktop-client-builds%2FBeta-Build-77-3-127%2Ftd-p%2F354660&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.dropboxforum.com/t5/Deskt...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2Fcg4r8u%2Fdropbox_added_support_for_zfs_ecryptfs_xfs_and%2F&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.reddit.com/r/linux/commen...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F07%2F25%2Fthe-new-version-of-deepin-linux-has-a-killer-feature-that-every-distribution-needs%2F%2353a8868f5b33&amp;v=Rm13QRauRb0&amp;redir_token=72cwkvbr2ka7l3NTDHuNAQd8zm98MTU3NTY5NzUwM0AxNTc1NjExMTAz&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.forbes.com/sites/jasoneva...
