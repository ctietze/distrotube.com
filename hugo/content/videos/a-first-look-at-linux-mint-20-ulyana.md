---
title: "A First Look At Linux Mint 20 'Ulyana' Cinnamon"
image: images/thumbs/0656.jpg
date: 2020-06-29T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Linux Mint"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-first-look-at-linux-mint-20-ulyana/d2d4bdc55a11380a2b9376c45c989237e4427d70?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Linux Mint 20, codenamed "Ulyana," was recently released so I thought I would take a quick first look at Linux Mint 20 with the Cinnamon desktop environment.  Linux Mint 20 has made headlines recently due to their decision to try to block installation of snaps.

REFERENCED:
+ https://www.linuxmint.com/rel_ulyana_cinnamon_whatsnew.php