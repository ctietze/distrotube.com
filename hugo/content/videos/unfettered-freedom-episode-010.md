---
title: "Unfettered Freedom, Ep. 10 - Youtube-dl, Linux Jobs, LBRY, Text Editors, Ubuntu, Fedora, NixOS" 
image: images/thumbs/0743.jpg
date: 2020-10-28T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/unfettered-freedom-ep-10-youtube-dl/dc543bcf5575328d97673a8323466b4948fe920c?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 2:12 - The music industry goes after youtube-dl; it is removed from GitHub.
+ 10:11 - Linux and open source jobs are hot right now.
+ 14:04 - LBRY has a marketing problem.
+ 17:30 - Six of the best text editors on Linux.
+ 24:41 - Distro releases: Ubuntu, Fedora and NixOS
+ 31:57 - Outro and a THANK YOU to the patrons!