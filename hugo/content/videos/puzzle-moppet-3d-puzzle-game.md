---
title: "Puzzle Moppet - 3D Puzzle Game for Linux (and Mac and Windows)"
image: images/thumbs/0065.jpg
date: Sun, 17 Dec 2017 15:24:02 +0000
author: Derek Taylor
tags: ["Gaming"]
---

#### VIDEO

{{< amazon src="Puzzle+Moppet+-+3D+Puzzle+Game+for+Linux+(and+Mac+and+Windows).mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at Puzzle Moppet, a 3D puzzle game that was brought to my attention by a channel viewer. I want to thank <a href="https://www.youtube.com/user/dlocklear01">https://www.youtube.com/user/dlocklear01</a> for introducing me to this little gem of a game. <a href="http://garnetgames.com/puzzlemoppet/">http://garnetgames.com/puzzlemoppet/</a>
