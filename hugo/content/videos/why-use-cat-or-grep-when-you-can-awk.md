---
title: "Why Use CAT Or GREP When You Can AWK?"
image: images/thumbs/0472.jpg
date: Mon, 02 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="Why+Use+Cat+Or+Grep+When+You+Can+Awk.mp4" >}}
&nbsp;

#### SHOW NOTES

Enough of the bloat!  You don't need to use cat, grep, head, sed, etc.  All you need is AWK!

#### REFERENCED:
+ ► https://www.gnu.org/software/gawk/manual/gawk.html
