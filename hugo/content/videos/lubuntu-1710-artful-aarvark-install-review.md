---
title: "Lubuntu 17.10 Artful Aarvark - Install and Review"
image: images/thumbs/0016.jpg
date: Wed, 18 Oct 2017 02:01:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Lubuntu", "LXDE"]
---

#### VIDEO

{{< amazon src="Lubuntu+17.10+Artful+Aarvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

A look at recently released Lubuntu 17.10 Artful Aardvark. Lubuntu is the flavor of Ubuntu that comes installed with the lightweight LXDE desktop environment. Fast install, minimal desktop, super-fast!
