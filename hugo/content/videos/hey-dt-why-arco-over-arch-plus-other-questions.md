---
title: "'Hey, DT. Why Arco Linux Instead Of Arch?' (Plus Other Questions Answered)"
image: images/thumbs/0690.jpg
date: 2020-08-10T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

{{< amazon src="Hey+DT+Why+Arco+Linux+Instead+Of+Arch+Plus+Other+Questions+Answered.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  I discuss fake DistroTube accounts on social media, my thoughts on PeerTube, my experience with LBRY, my thoughts on Arco vs Arch vs Artix, and what YouTubers have influenced my life.
