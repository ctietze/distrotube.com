---
title: "Command Line Tricks With Caret And Exclamation"
image: images/thumbs/0524.jpg
date: 2020-01-24T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Command+Line+Tricks+With+Caret+And+Exclamation.mp4" >}}
&nbsp;

#### SHOW NOTES

This quick video will show you how to do text replacement in the shell using the caret (^) symbol and how to use the exclamation (!) symbol to run past commands from your history.

REFERENCED:
+ https://www.gnu.org/software/bash/manual/bash.html