---
title: "Search For Patterns Within Files With Grep"
image: images/thumbs/0674.jpg
date: 2020-07-21T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Search+For+Patterns+Within+Files+With+Grep.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the most important command line utilities that every Linux user should know is Grep.  Grep searches one or more input files for lines containing a match to a specified pattern. By default, Grep outputs the matching lines but it has a ton of flags and options available. 


ERRATA:
I mentioned that Grep doesn't like binaries.  That's not entirely true.  There is a way to grep a binary as if it were a text file by using the "-a" flag.  One thing to note though is many binaries are just one gigantic line so a lot of the time, all you are going to get is the equivalent of a "cat binary-file".


REFERENCED:
+ https://www.gnu.org/software/grep/ - GNU Grep