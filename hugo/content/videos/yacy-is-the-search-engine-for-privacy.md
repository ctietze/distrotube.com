---
title: "Yacy Is The Search Engine That Respects Your Privacy" 
image: images/thumbs/0764.jpg
date: 2020-11-28T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", "Privacy"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/yacy-is-the-search-engine-that-respects/fd5aad04cf784ac9a03022fe0d795f2a2d9d786d?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Yacy is a decentralized, peer-to-peer web search engine.  All users are equal with no central controlling authority.  Access to the search functions is made by a locally running web server which provides a search box to enter search terms, and returns search results in a similar format to other popular search engines.   Yacy is available on Windows, Mac and Linux

REFERENCED:
+ https://yacy.net/

