---
title: "Enabling A Firewall Is Easy In Linux" 
image: images/thumbs/0738.jpg
date: 2020-10-21T12:23:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/enabling-a-firewall-is-easy-in-linux/1156d8536c9d9bcb83eef548a419301f0ca58a00?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I am going to show you how to install and enable the Uncomplicated Firewall (ufw) and how to add and delete rules for it.  Ufw is a very easy-to-use command line utility, and for those that want a graphical tool, gufw is available as well.