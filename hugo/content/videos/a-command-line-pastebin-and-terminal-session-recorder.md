---
title: "A Command Line Pastebin and a Terminal Session Recorder - Termbin & Asciinema"
image: images/thumbs/0363.jpg
date: Sat, 09 Mar 2019 23:14:04 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-command-line-pastebin-and-a-terminal/c4148eb8e37e1e54c4cf30c9a280d05b790ea53a?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm sharing with you a neat command line "pastebin" alternative  called termbin , as well a terminal session recorder with a horrible  name--asciinema! 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=http%3A%2F%2Ftermbin.com%2F&amp;event=video_description&amp;v=nAUohhEJ56M&amp;redir_token=0AKyBcFwA1FB6IPANudVhh9Jyet8MTU1MzY0MjA3MkAxNTUzNTU1Njcy" target="_blank">http://termbin.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fasciinema.org%2F&amp;event=video_description&amp;v=nAUohhEJ56M&amp;redir_token=0AKyBcFwA1FB6IPANudVhh9Jyet8MTU1MzY0MjA3MkAxNTUzNTU1Njcy" target="_blank">https://asciinema.org/
