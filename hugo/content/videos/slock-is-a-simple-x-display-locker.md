---
title: "Slock Is A Simple X Display Locker"
image: images/thumbs/0628.jpg
date: 2020-05-21T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Slock+Is+A+Simple+X+Display+Locker.mp4" >}}
&nbsp;

#### SHOW NOTES

Slock is a simple X display locker from Suckless.org.  It is simple to install, modify and use.  It is well suited to serve as your screen locker if you want something minimal and stable.

REFERENCED:
+ https://tools.suckless.org/slock/