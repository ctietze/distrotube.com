---
title: "A Quick Look At CloverOS GNU/Linux"
image: images/thumbs/0248.jpg
date: Sun, 22 Jul 2018 00:32:16 +0000
author: Derek Taylor
tags: ["Distro Reviews", "CloverOS"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-cloveros-gnu-linux/fd07b407a1756230a18c4394f7dfd1e136eaa5c5?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

A quick look at CloverOS GNU/Linux--a Gentoo-based Linux distro with a super quick install. https://cloveros.ga/
