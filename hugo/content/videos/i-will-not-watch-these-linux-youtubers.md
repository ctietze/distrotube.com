---
title: "I Will Not Watch These Linux YouTubers" 
image: images/thumbs/0752.jpg
date: 2020-11-10T12:23:40+06:00
author: Derek Taylor
tags: ["YouTube", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/i-will-not-watch-these-linux-youtubers/ef080b38d26b3741cfcf3ba1e2a70b450610c720?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

In this rant, I talk about why I don't watch much Linux YouTube content. I talk about three categories of YouTubers that I especially take issue with, and how these kinds of YouTubers are completely missing the mark with their coverage of Linux.
