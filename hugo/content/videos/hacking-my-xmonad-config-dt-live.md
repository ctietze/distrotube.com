---
title: "Hacking My Xmonad Config - DT LIVE"
image: images/thumbs/0654.jpg
date: 2020-06-24T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "tiling window manager", "xmonad"]
---

#### VIDEO

{{< amazon src="Hacking+My+Xmonad+Config+-+DT+LIVE-6ingR5O9xe4.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm going to add a bunch of custom configuration to my Xmonad config--LIVE!  This will give you some idea of the process I go through, such as reading the documentation, copying and pasting what I find, trying to fix errors in the code, etc. 