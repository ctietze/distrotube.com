---
title: "Surfraw Lets You Search The Web...From The Terminal"
image: images/thumbs/0285.jpg
date: Wed, 19 Sep 2018 19:36:10 +0000
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="Surfraw+Lets+You+Search+The+Web...From+The+Terminal.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a look at a neat little command line utility that allows you  to search 100 different popular sites.  The program, named Surfraw, was  originally created by Julian Assange of WikiLeaks fame. 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=C0KXrK-p4iPx6Tp6Hvg0v8j-EgV8MTU1MzQ1NjE4OEAxNTUzMzY5Nzg4&amp;v=hP8-PItT_UI&amp;q=https%3A%2F%2Fgithub.com%2Fkisom%2Fsurfraw&amp;event=video_description" target="_blank">https://github.com/kisom/surfraw

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=C0KXrK-p4iPx6Tp6Hvg0v8j-EgV8MTU1MzQ1NjE4OEAxNTUzMzY5Nzg4&amp;v=hP8-PItT_UI&amp;q=https%3A%2F%2Fwww.ostechnix.com%2Fsurfraw-commandline-interface-popular-search-engines-100-websites%2F&amp;event=video_description" target="_blank">https://www.ostechnix.com/surfraw-com...
