---
title: "Why Do You Need All That Hardware?" 
image: images/thumbs/0760.jpg
date: 2020-11-22T12:23:40+06:00
author: Derek Taylor
tags: ["Hardware", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/why-do-you-need-all-that-hardware/bee75ca9c0fe493073601b85c664456eb33c1927?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

I see this way too often in the comments of my videos and just browsing the Internet.  Someone will share the specs of their  beefy computer and then someone will immediately ask "Why ya need all that?!"

