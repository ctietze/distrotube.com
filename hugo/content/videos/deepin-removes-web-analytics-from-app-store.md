---
title: "Deepin To Remove Web Analytics From App Store Due To Spyware Claims"
image: images/thumbs/0247.jpg
date: Fri, 20 Jul 2018 00:30:31 +0000
author: Derek Taylor
tags: ["Deepin", "Security"]
---

#### VIDEO

{{< amazon src="Deepin+To+Remove+Web+Analytics+From+App+Store+Due+To+Spyware+Claims.mp4" >}}
&nbsp;

#### SHOW NOTES

Breaking news. Deepin announces they will remove the CNZZ statistics from their App Store. 

DEEPIN STATEMENT: https://www.deepin.org/en/2018/07/20/statement-on-canceling-cnzz-statistics-in-the-deepin-app-store/

CHECK OUT QUIDSUP: https://www.youtube.com/user/quidsup
