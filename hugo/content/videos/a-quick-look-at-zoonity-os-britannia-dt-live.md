---
title: "A Quick Look At Zoonity OS Britannia - DT LIVE"
image: images/thumbs/0262.jpg
date: Sat, 04 Aug 2018 19:22:29 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Zoonity OS", "Live Stream"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-zoonity-os-britannia-dt/8a5c91744ad63360834228ddee2ab1cb2d9eb341?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this impromptu live stream, I'm going to take a quick first look at Zoonity OS Britannia.  It is a Xubuntu-based distro that sports the Xfce desktop but made to look like Unity.  Will it be awesome or just another distro? 
