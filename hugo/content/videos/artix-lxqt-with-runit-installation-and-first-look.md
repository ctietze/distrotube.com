---
title: "Artix LXQt with Runit - Installation and First Look"
image: images/thumbs/0292.jpg
date: Fri, 05 Oct 2018 19:54:25 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Artix"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/artix-lxqt-with-runit-installation-and/43c5c6d360042c1f5ee4da315a46cb4b03ea7e25?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I'm taking a quick look at Artix Linux with the LXQt desktop and the 
runit init system.  Artix is an Arch-based Linux distro that is focused 
on not using systemd.  

+ https://artixlinux.org/
