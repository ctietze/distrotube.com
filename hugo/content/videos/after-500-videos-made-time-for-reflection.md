---
title: "After 500 Videos Made, A Time For Reflection"
image: images/thumbs/0426.jpg
date: Sat, 27 Jul 2019 05:49:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/after-500-videos-made-a-time-for/64e3bfb36e664111f1438425a0d9f98a69095c80?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Tomorrow I plan to do a live stream to mark my 500th video on YouTube. What a ride! What a journey! I want to thank all you guys for your support. I am honored to call each and every one of you, my Linux brothers and sisters.
