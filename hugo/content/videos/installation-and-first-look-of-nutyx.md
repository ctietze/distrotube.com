---
title: "Installation And First Look Of NuTyX" 
image: images/thumbs/0774.jpg
date: 2020-12-16T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "NuTyX"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/installation-and-first-look-of-nutyx/0fb3362391e7bac486aa5f07a41762037a5b61da?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

I'm taking a quick first look at a Linux distribution that I haven't tried before.  That distribution is NuTyX.  It's country of origin is Switzerland, and the distro is based on Linux From Scratch.  It has its own package manager called "cards".  It also uses BusyBox. 

REFERENCED:
+ https://nutyx.org/en/ - NuTyX