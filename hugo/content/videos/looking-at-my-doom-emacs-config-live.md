---
title: "Looking At My Doom Emacs Config - DT LIVE" 
image: images/thumbs/0773.jpg
date: 2020-12-14T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Emacs"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/looking-at-my-doom-emacs-config-dt-live/1a2ad9ffec9a079043c5651feb9f7e5a5dc87575?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

I've been playing a bit with my Doom Emacs configuration, so I thought I would go through my config files and explain what I'm doing.  This could prove to be quite educational to those of you that are new to Emacs in general and especially Doom Emacs.   

REFERENCED:
+ https://github.com/hlissner/doom-emacs - Doom Emacs