---
title: "Peppermint OS 8 Install & Review"
image: images/thumbs/0055.jpg
date: Sat, 09 Dec 2017 14:49:02 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Peppermint"]
---

#### VIDEO

{{< amazon src="Peppermint+OS+8+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at the Lubuntu-based Linux distribution called Peppermint OS. Peppermint OS 8 sports a hybrid desktop environment that contains elements of both the LXDE and XFCE desktop environments. Lightweight, designed for older computers or "speed freaks" who want their modern systems to fly! <a href="https://peppermintos.com/">https://peppermintos.com/</a>
