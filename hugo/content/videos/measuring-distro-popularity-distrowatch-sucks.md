---
title: "Measuring Distro Popularity And Why Distrowatch's Page Hit Rankings Suck."
image: images/thumbs/0075.jpg
date: Sat, 30 Dec 2017 15:59:26 +0000
author: Derek Taylor
tags: ["DistroWatch"]
---

#### VIDEO

{{< amazon src="Measuring+Distro+Popularity+And+Why+Distrowatchs+Page+Hit+Rankings+Suck..mp4" >}}
&nbsp;

#### SHOW NOTES

I go on a rather long rant about the flaws in a lot of the methods of trying to determine Linux distro popularity. Referenced in the video: <a href="https://distrowatch.com/">https://distrowatch.com/</a> <a href="https://www.omgubuntu.co.uk/2017/12/how-popular-was-ubuntu-in-2017">https://www.omgubuntu.co.uk/2017/12/how-popular-was-ubuntu-in-2017</a> <a href="https://stats.wikimedia.org/wikimedia/squids/SquidReportOperatingSystems.htm">https://stats.wikimedia.org/wikimedia/squids/SquidReportOperatingSystems.htm</a>     
