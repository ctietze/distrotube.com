---
title: "Mabox 20.02 Installation and First Look"
image: images/thumbs/0566.jpg
date: 2020-03-10T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Mabox", "Openbox"]
---

#### VIDEO

{{< amazon src="Mabox+2002+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Mabox is a Linux distro that is based on Manjaro, with a preconfigured lightweight Openbox desktop and some specially developed programs and tools. Light and fast, it is great on your newer computers as well as older, weaker hardware.

REFERENCED:
+ https://maboxlinux.org/
