---
title: "Stumpwm Is One Strange Window Manager"
image: images/thumbs/0466.jpg
date: Tue, 19 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "stumpwm"]
---

#### VIDEO

{{< amazon src="Stumpwm+Is+One+Strange+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been playing around with stumpwm for a week or two and I have to say: it's the strangest and most frustrating window manager I've ever used (yes, GNOME included!).  

#### REFERENCED:
+ ► http://stumpwm.github.io/
