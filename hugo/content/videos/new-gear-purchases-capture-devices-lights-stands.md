---
title: "New Gear Purchases - Capture Devices, Lights and Mic Stands"
image: images/thumbs/0502.jpg
date: 2020-01-08T12:22:40+06:00
author: Derek Taylor
tags: ["Audio", "Video"]
---

#### VIDEO

{{< amazon src="New+Gear+Purchases+-Capture+Devices%2C+Lights+and+Mic+Stands.mp4" >}}
&nbsp;

#### SHOW NOTES

This is a quick video discussing a few gear purchases that I've made in the last month or two.  This includes video capture devices, lighting and stands, and microphone stands and extensions.

REFERENCED IN THE VIDEO:
+ ► El Gato Cam Link - https://amzn.to/35Bf7Wg
+ ► Magewell Capture Device - https://amzn.to/2QWJjpL
+ ► Gator Frameworks Mic Stand - https://amzn.to/2s5Smw6
+ ► On-Stage Posi-Lok Mini Mic Arm - https://amzn.to/2uywGd7
+ ► Fovitec Lights & Stand (2-pack) - https://amzn.to/2N7uhwg

&nbsp;
OTHER EQUIPMENT I USE:
+ ► Blue Ember - https://amzn.to/2DLJRZh
+ ► Blue Baby Bottle SL - https://amzn.to/2RkZRcT
+ ► Scarlett 2i2 Interface - https://amzn.to/2YjLB5A
+ ► Panasonic Lumix G7 - https://amzn.to/2N9PBRE
