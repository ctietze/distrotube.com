---
title: "Day 12 (dwm) - Status Bar Configuration"
image: images/thumbs/0344.jpg
date: Fri, 01 Feb 2019 22:43:49 +0000
author: Derek Taylor
tags: ["tiling window managers", "dwm"]
---

#### VIDEO

{{< amazon src="Day+12+(dwm)+-+Status+Bar+Config.mp4" >}}
&nbsp;

#### SHOW NOTES

It's day 12 of living in dwm.   Configuring the dwm status bar can be a  bit challenging for the new user, so today I'm going to go into a bit  more detail regarding the built-in dwm panel. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fsuckless.org%2F&amp;redir_token=-CO1aU8qq2y5VjB7HniJs-PkXFF8MTU1MzY0MDI0NEAxNTUzNTUzODQ0&amp;event=video_description&amp;v=M9drMlXRfg8" target="_blank">https://suckless.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles&amp;redir_token=-CO1aU8qq2y5VjB7HniJs-PkXFF8MTU1MzY0MDI0NEAxNTUzNTUzODQ0&amp;event=video_description&amp;v=M9drMlXRfg8" target="_blank">https://gitlab.com/dwt1/dotfiles
