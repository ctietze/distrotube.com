---
title: "AntiX 17 First Impression Install & Review"
image: images/thumbs/0048.jpg
date: Tue, 05 Dec 2017 14:37:46 +0000
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/antix-17-first-impression-install-review/5428f1f50086a9d9ac74d359afd3f5565aed3be8?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I'm taking a quick look at AntiX 17, a Debian-based Linux distro that is lightweight, offers the user a choice of three different Debian branches, and comes pre-installed with a number of different window managers. <a href="https://antixlinux.com/">https://antixlinux.com/</a>
