---
title: "The APT Package Manager in Debian and Ubuntu"
image: images/thumbs/0130.jpg
date: Fri, 02 Mar 2018 21:17:55 +0000
author: Derek Taylor
tags: ["Command Line", "Terminal"]
---

#### VIDEO

{{< amazon src="The+APT+Package+Manager+in+Debian+and+Ubuntu.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I cover a few of the most common commands you will need to use with the APT package manager that is used in Debian, Ubuntu and those distros based on Debian and Ubuntu. 
