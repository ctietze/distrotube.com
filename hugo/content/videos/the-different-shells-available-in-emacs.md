---
title: "The Different Shells Available In Emacs" 
image: images/thumbs/0733.jpg
date: 2020-10-15T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", "terminal"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-different-shells-available-in-emacs/30f152c3c4634c0a777f0682b53e02c30f5c0c78?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Emacs can be overwhelming to the new user.  There is so much stuff inside Emacs.  Take the various shells and terminal emulators inside Emacs.  Why are there so many?  What's the differences between them?