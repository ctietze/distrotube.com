---
title: "Add Bling to Your Terminal With Neofetch and Powerline Shell"
image: images/thumbs/0223.jpg
date: Fri, 01 Jun 2018 23:48:21 +0000
author: Derek Taylor
tags: ["terminal", "commmand line"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/add-bling-to-your-terminal-with-neofetch/a33dfef0d119678d2e6794c0a74a7c1ccbdef1e9?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I going to install a couple of programs to help make our shell a little bit sexier. 

+ https://github.com/dylanaraps/neofetch 
+ https://github.com/b-ryan/powerline-shell
