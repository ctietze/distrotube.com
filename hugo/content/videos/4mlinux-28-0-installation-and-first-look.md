---
title: "4MLinux 28.0 - Installation and First Look"
image: images/thumbs/0364.jpg
date: Mon, 11 Mar 2019 23:15:22 +0000
author: Derek Taylor
tags: ["Distro Reviews", "4MLinux"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/4mlinux-28-0-installation-and-first-look/7253dcb2fd328699596a635bdf17b9ef93efb397?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm taking at an independent Linux distribution called 4MLinux.   It is a lightweight, minimal distro from Poland.  Great for a live  system rescue CD or USB stick! 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=http%3A%2F%2F4mlinux.com%2F&amp;redir_token=uxTIdvNMjWWevMsrqP6-YQM4yCR8MTU1MzY0MjEyNEAxNTUzNTU1NzI0&amp;v=0FbludoWeII" target="_blank">http://4mlinux.com/
