---
title: "Unfettered Freedom, Ep. 8 - ESR Predictions, Respondus, DRM, Webmail Providers, All Distros Are Good" 
image: images/thumbs/0725.jpg
date: 2020-09-30T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/unfettered-freedom-ep-8-esr-predictions/b8c0db295d22d3de203d19115f4ecedca3a98a26?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 2:04 - Eric S. Raymond has some predictions about Windows 10 and Linux.
+ 9:04 - The Respondus Lockdown Browser.
+ 15:26 - DRM is designed to limit your rights.
+ 19:31 - Are there any "free as in freedom" webmail providers?
+ 23:08 - Every distro is awesome! (or is it?)
+ 30:01 - Outro and a THANK YOU to the patrons!