---
title: "Olivia Cloud Music Player"
image: images/thumbs/0523.jpg
date: 2020-01-23T12:22:40+06:00
author: Derek Taylor
tags: ["YouTube", "Audio"]
---

#### VIDEO

{{< amazon src="Olivia+Cloud+Music+Player.mp4" >}}
&nbsp;

#### SHOW NOTES

Olivia is an elegant cloud music and video player for Linux.  It is available as a Snap package, and it can be found in the Arch User Repository (AUR).   

REFERENCED:
+ https://github.com/keshavbhatt/olivia