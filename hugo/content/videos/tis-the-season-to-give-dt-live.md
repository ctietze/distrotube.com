---
title: "'Tis The Season To Give - DT LIVE"
image: images/thumbs/0501.jpg
date: 2019-12-20T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="'Tis+The+Season+To+Give+-+DT+LIVE-H28xrJwHVW8.mp4" >}}
&nbsp;

#### SHOW NOTES

The season of giving is upon us.  So let's discuss some of our favorite FOSS projects to donate money to this holiday season.  I will share some of my favorites and you guys in the chat can share your favorites.
