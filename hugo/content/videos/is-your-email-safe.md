---
title: "Is Your Email Safe?"
image: images/thumbs/0565.jpg
date: 2020-03-09T12:22:40+06:00
author: Derek Taylor
tags: ["Email", ""]
---

#### VIDEO

{{< amazon src="Is+Your+Email+Safe.mp4" >}}
&nbsp;

#### SHOW NOTES

Email is inherently unsafe.  Email was not designed with security or privacy in mind because it was created before the explosion of the Internet.  But there are a few things you can do to help guard against malicious attacks.
