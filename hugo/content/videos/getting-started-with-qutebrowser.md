---
title: "Getting Started With Qutebrowser"
image: images/thumbs/0612.jpg
date: 2020-05-02T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "Qutebrowser"]
---

#### VIDEO

{{< amazon src="Getting+Started+With+Qutebrowser.mp4" >}}
&nbsp;

#### SHOW NOTES

Qutebrowser is a keyboard-focused browser that uses vim-like bindings with a minimal interface that gets out of the user's way. It’s written in Python and is free software, licensed under the GPL.  

REFERENCED:
+ https://qutebrowser.org/