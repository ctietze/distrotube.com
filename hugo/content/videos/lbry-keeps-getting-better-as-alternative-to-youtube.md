---
title: "LBRY Keeps Getting Better As An Alternative To YouTube" 
image: images/thumbs/0727.jpg
date: 2020-10-03T12:23:40+06:00
author: Derek Taylor
tags: ["LBRY", "YouTube"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/lbry-keeps-getting-better-as-an/d8b764a757fa397d26efd88389ecb221adba25bb?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

When I joined LBRY at the beginning of 2020, I never could have imagined the kind of growth that the platform would receive.  In just a few months, there are so many more creators and viewers on LBRY, the website has undergone some improvements, and the desktop and mobile apps keep better.  Also, due a number or reasons, video content creation has been my only source of income in 2020, and LBRY has a been a big part of that.  I'm not sure I'd be getting by without LBRY.