---
title: "Hey DT! My Qt Programs Don't Look Good. Plus Other Comments" 
image: images/thumbs/0765.jpg
date: 2020-11-30T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/hey-dt-my-qt-programs-don-t-look-good/ceb643a4d7012c71719645af616a59f37a35e2d2?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

In this lengthy rant video, I address a few questions and comments that I've been receiving from viewers.  Some of the topics include why my '"ls" looks different, how I record TTY, how am I getting along with EXWM, is YouTube still my only job, why I use Kdenlive over Davinci Resolve, and a couple of questions regarding virtual machines.

