---
title: "How To Make Your Tiling Window Manager Look Amazing"
image: images/thumbs/0695.jpg
date: 2020-08-17T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", ""]
---

#### VIDEO

{{< amazon src="How+To+Make+Your+Tiling+Window+Manager+Look+Amazing.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been getting a lot of questions from new tiling window manager users asking how to make their setups look amazing--like those gorgeous screenshots you see on r/unixporn.  So I briefly cover a few of the basics when it comes to theming your tiling window manager desktops.

REFERENCED:
+ https://gitlab.com/dwt1 - DT's GitLab repositories
+ https://www.reddit.com/r/unixporn/ - The Unixporn subreddit