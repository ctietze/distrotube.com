---
title: "Trapped Inside Of Vim? Drastic Methods Are Sometimes Required!"
image: images/thumbs/0635.jpg
date: 2020-05-30T12:23:40+06:00
author: Derek Taylor
tags: ["Vim", ""]
---

#### VIDEO

{{< amazon src="Trapped+Inside+Of+Vim+Drastic+Methods+Are+Sometimes+Required.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm getting far too many people messaging me that they launched Vim and can't seem to escape out of it.  I get it.  Exiting Vim is hard.  Here are some proven methods guaranteed work every time!  Some of these are  "nuclear" options though.

REFERENCED:
+ https://github.com/caseykneale/VIMKiller