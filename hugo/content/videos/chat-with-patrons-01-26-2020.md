---
title: "Chat with Patrons (January 26, 2020)"
image: images/thumbs/0528.jpg
date: 2020-01-26T12:22:40+06:00
author: Derek Taylor
tags: ["Chat With Patrons", "", ""]
---

#### VIDEO

{{< amazon src="Chat+with+Patrons+(January+26%2C+2020)-HYJKWmN5bbQ.mp4" >}}
&nbsp;

#### SHOW NOTES

NOTE:  This is a re-upload.  I had to take the original video down and do some minor editing. This was the monthly Chat With Patrons for January, 2020.  This was originally recorded on January 26, 2020.  Thanks to all of my patrons!os has been Solus Budgie.  I have found Solus to be a polished and stable distro.  Version 4.1 of Solus was just released.  How does it measure up?

REFERENCED:
+ https://www.patreon.com/distrotube