---
title: "Noob Questions and Comments - ANSWERED!"
image: images/thumbs/0333.jpg
date: Fri, 11 Jan 2019 20:08:03 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Noob+Questions+and+Comments+ANSWERED.mp4" >}}
&nbsp;

#### SHOW NOTES

I get a lot of new subscribers every month.  As "noobs" to the channel, they tend to ask the same questions and make the same comments over and over and over again.  So here is a response to ten of these noob-type questions and comments.
