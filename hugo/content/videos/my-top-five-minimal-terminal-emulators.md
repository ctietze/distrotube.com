---
title: "My Top Five Minimal Terminal Emulators"
image: images/thumbs/0623.jpg
date: 2020-05-15T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="My+Top+Five+Minimal+Terminal+Emulators.mp4" >}}
&nbsp;

#### SHOW NOTES

In this raI've used many terminal emulators on Linux, and the ones I typically use are the more "minimal" ones.  Light, fast and without all the bloat!  So you won't find unnecessary cruft here like tabs and ligature support in these terminals.

REFERENCED:
+ https://invisible-island.net/xterm/
+ http://software.schmorp.de/pkg/rxvt-unicode.html
+ https://github.com/thestinger/termite
+ https://st.suckless.org/
+ https://github.com/alacritty/alacrittyther lengthy video, I go over the basics of getting started with Xmonad.  I install Xmonad on Ubuntu 20.04 and show some of the basics as far as configuration.   I also show you how to use the Haskell documentation.  And I briefly show you how to install and configure Xmobar. 