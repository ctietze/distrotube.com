---
title: "My First Time Looking At OpenIndiana"
image: images/thumbs/0279.jpg
date: Mon, 10 Sep 2018 18:56:41 +0000
author: Derek Taylor
tags: ["Distro Reviews", "OpenIndiana"]
---

#### VIDEO

{{< amazon src="My+First+Time+Looking+At+OpenIndiana.mp4" >}}
&nbsp;

#### SHOW NOTES

Taking a look at OpenIndiana for the first time.  It is a Unix operating system based on OpenSolaris.

<a href="https://www.youtube.com/redirect?redir_token=cjPjHSC5PoV7kJXzk06BoJmRgD98MTU1MzQ1Mzk4NUAxNTUzMzY3NTg1&amp;q=https%3A%2F%2Fwww.openindiana.org%2F&amp;v=962B7sgHauY&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://www.openindiana.org/
