---
title: "Vim Can Save You Hours Of Work" 
image: images/thumbs/0781.jpg
date: 2020-12-29T12:23:40+06:00
author: Derek Taylor
tags: ["Vim", "Productivity"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/vim-can-save-you-hours-of-work/283d62e259d3fe1fb84be18768adc49e5c0f82b8?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

I sometimes get people asking "Is learning Vim worth it?"  The answer to that question is a resounding "YES!"  Vim can save you so much time editing text once you learn some of the advanced features available within it.

REFERENCED:
+ https://www.vim.org/