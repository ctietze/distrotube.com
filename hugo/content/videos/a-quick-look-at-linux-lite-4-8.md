---
title: "A Quick Look At Linux Lite 4.8"
image: images/thumbs/0519.jpg
date: 2020-01-19T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Linux Lite"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-linux-lite-4-8/3c74cccf14824486fdca2a42410f198eef7e60a0?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I'm taking the recently released Linux Lite 4.8 for spin.  This is an Ubuntu-based distro that uses the Xfce desktop environment.  It is light and fast.  And this version is aimed at winning over those folks that are still using Windows 7.

REFERENCED:
+ https://www.linuxliteos.com/