---
title: "Command Mode, Ranges and 'g' in Vim"
image: images/thumbs/0624.jpg
date: 2020-05-16T12:23:40+06:00
author: Derek Taylor
tags: ["Vim", ""]
---

#### VIDEO

{{< amazon src="Command+Mode%2C+Ranges+and+'g'+in+Vim.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm going to briefly discuss a few things you can do with command mode, ranges, and especially the 'g' command in Vim.  'g' doesn't stand for 'gangsta' but it should!  