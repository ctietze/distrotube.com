---
title: "Taking Into Account, Ep. 25 - Systemd, VLC, MongoDB, SuperTuxKart, FSF"
image: images/thumbs/0337.jpg
date: Thu, 17 Jan 2019 22:30:51 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+25.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=44s">0:44 Systemd, the controversial init system and service manager, has  three security holes. 

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=320s">5:20 Open source VLC Media Player app reaches a major milestone as it passed 3 billion downloads.  

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=568s">9:28 MongoDB "open-source" Server Side Public License (SSPL) rejected by Red Hat and Fedora. 

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=960s">16:00 SuperTuxKart, the open source Mario Kart clone, launches beta with online multiplayer support. 

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=1127s">18:47 The FSF end of year fundraiser was successful. The FSF is 5,000 members strong. Make it 5,001!  :D 

<a href="https://www.youtube.com/watch?v=i7NtXYaXq1A&amp;t=1430s">23:50 Where do I find the articles and blog posts that I use on Taking Into Account and my other videos? 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fnew-linux-systemd-security-holes-uncovered%2F&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://www.zdnet.com/article/new-lin...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fnews.softpedia.com%2Fnews%2Fvlc-media-player-passes-3-billion-downloads-mark-airplay-support-coming-soon-524506.shtml&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://news.softpedia.com/news/vlc-m...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fmongodb-open-source-server-side-public-license-rejected%2F&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://www.zdnet.com/article/mongodb...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fwww.mongodb.com%2Flicensing%2Fserver-side-public-license&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://www.mongodb.com/licensing/ser...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fopensource.org%2Fosd-annotated&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://opensource.org/osd-annotated

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fbetanews.com%2F2019%2F01%2F11%2Fsupertuxkart-mario-kart-linux%2F&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://betanews.com/2019/01/11/super...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=i7NtXYaXq1A&amp;event=video_description&amp;q=https%3A%2F%2Fwww.fsf.org%2Fblogs%2Fcommunity%2Fthe-fsf-is-5-000-members-strong-thanks-to-you&amp;redir_token=-yUuO1JBUBtbsT3KjYG2T35Nhyl8MTU1MzYzOTUxMUAxNTUzNTUzMTEx" target="_blank">https://www.fsf.org/blogs/community/t...
