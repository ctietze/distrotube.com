---
title: "The Obscure Window Manager Project"
image: images/thumbs/0101.jpg
date: Mon, 29 Jan 2018 01:31:43 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Obscure+Window+Manager+Project.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I discuss a new project I'm thinking of starting--reviewing a dozen different window managers, some of them I know a little about, some of them I know nothing about. I am also considering possibly uploading the virtual machine images to SourceForge so you guys can download them and import them into Virtualbox. Would love some feedback on this!     Window managers I think I will cover: awesome fluxbox fvwm herbstluftwm i3-gaps icewm jwm openbox pekwm qtile xorg-twm xmonad
