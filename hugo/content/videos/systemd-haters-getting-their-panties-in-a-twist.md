---
title: "Systemd Haters Getting Their Panties In A Twist"
image: images/thumbs/0392.jpg
date: Sat, 11 May 2019 14:28:00 +0000
author: Derek Taylor
tags: ["sytemd", ""]
---

#### VIDEO

{{< amazon src="Systemd+Hater+Getting+Their+Panties+In+A+Twist.mp4" >}}
&nbsp;

#### SHOW NOTES

Is hating on systemd just a meme? Or, are there legit reasons to "hate" on systemd?
&nbsp;

#### REFERENCED:
+ https://www.youtube.com/watch?v=IglXPVJ98t0&amp;t=1s
