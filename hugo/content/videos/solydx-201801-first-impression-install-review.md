---
title: "SolydX 201801 First Impression Install & Review"
image: images/thumbs/0093.jpg
date: Sat, 20 Jan 2018 01:19:07 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SolydX", "XFCE"]
---

#### VIDEO

{{< amazon src="SolydX+201801+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a brief look at SolydX--a Debian-based Linux distribution that features the Xfce desktop environmen (there is also a flavor called SolydK that features the KDE desktop environment). Orginally started as a respin of Linux Mint Debian Edition after Mint dropped the Debian-based Xfce flavor. <a href="https://solydxk.com/">https://solydxk.com/</a>
