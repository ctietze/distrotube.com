---
title: "The Vim Tutorial - Part Two - More Commands"
image: images/thumbs/0346.jpg
date: Tue, 05 Feb 2019 22:46:24 +0000
author: Derek Taylor
tags: ["Vim", "terminal"]
---

#### VIDEO

{{< amazon src="The+Vim+Tutorial+Part+Two+More+Commands.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's continue forward in the Vim Tutorial.  Today, I will expand on 
some commands I discussed in Part One, and I will also introduce a few 
new commands.

PART ONE: <a href="https://www.youtube.com/watch?v=ER5JYFKkYDg">https://www.youtube.com/watch?v=ER5JY...
