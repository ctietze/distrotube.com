---
title: "Free And Open Chat Sunday - The Channel Turns One Year Old"
image: images/thumbs/0294.jpg
date: Sun, 07 Oct 2018 23:49:45 +0000
author: Derek Taylor
tags: ["Free And Open Chat Sunday", ""]
---

#### VIDEO

{{< amazon src="Free+And+Open+Chat+Sunday+-+The+Channel+Turns+One+Year+Old.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's show is a free and open chat. Today also is the one year mark for this channel--what a ride it's been! You can join me live on the video chat using Zoom, which is available on Windows, Linux and Mac.
