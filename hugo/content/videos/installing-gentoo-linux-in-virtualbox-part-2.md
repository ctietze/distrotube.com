---
title: "Installing Gentoo Linux in Virtualbox (Part 2)"
image: images/thumbs/0207.jpg
date: Tue, 15 May 2018 23:21:43 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Gentoo"]
---

#### VIDEO

{{< amazon src="Installing+Gentoo+Linux+in+Virtualbox+(Part+2).mp4" >}}
&nbsp;

#### SHOW NOTES

Had a day off from work so I wanted to just relax today. So I thought "I should install Gentoo!" This installation is split up into two videos. This is Part 2. https://gentoo.org/
