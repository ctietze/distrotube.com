---
title: "The Battle For Wesnoth - DT LIVE"
image: images/thumbs/0609.jpg
date: 2020-04-28T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Gaming"]
---

#### VIDEO

{{< amazon src="The+Battle+For+Wesnoth+-+DT+LIVE-IZpaR0Aqhc8.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight I felt like playing one of my favorite free  and open source games--The Battle For Wesnoth.  It is a turn-based strategy with some fantastic gameplay and an original soundtrack that is truly incredible:

REFERENCED:
+ https://www.wesnoth.org/