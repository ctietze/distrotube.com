---
title: "Live Feb 25, 2018 - Goofing around in Qtile, installing CLI apps"
image: images/thumbs/0125.jpg
date: Sun, 25 Feb 2018 21:10:45 +0000
author: Derek Taylor
tags: ["Live Stream", "qtile", "Command Line"]
---

#### VIDEO

{{< amazon src="Live+Feb+25%2C+2018+-+Goofing+around+in+Qtile%2C+installing+CLI+apps.mp4" >}}  
&nbsp;

#### SHOW NOTES

I am going to try to pimp out my Qtile config and install a few different CLI and terminal-based programs---because nothing says "gangsta!" like rocking a tiling window manager and only using terminal apps. May discuss some news, respond to viewer comments and rant in a nonsensical sort of way. Alot of the configs I used in this video can be found at my GitHub page: <a href="https://gitlab.com/dwt1">https://gitlab.com/dwt1</a>
