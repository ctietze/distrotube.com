---
title: "My 2019 Window Manager Will Be..."
image: images/thumbs/0323.jpg
date: Fri, 28 Dec 2018 19:41:37 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm"]
---

#### VIDEO

{{< amazon src="My+2019+Window+Manager+Will+Be.mp4" >}}
&nbsp;

#### SHOW NOTES

I spent almost all of 2018 living in Openbox (in the early part of the 
year) and Qtile.  It's time for me to hop---that is, window manager hop!
  I created a poll on YouTube for you, the viewer, to help me decide on 
my next window manager to use on my main production machine.  I have 
listed 5 tiling window managers to choose from: awesome, bspwm, 
herbstluftwm, i3 and xmonad.
