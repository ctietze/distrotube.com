---
title: "Five Reasons Linux is Superior to Windows"
image: images/thumbs/0168.jpg
date: Wed, 04 Apr 2018 22:19:13 +0000
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Five+Reasons+Linux+is+Superior+to+Windows.mp4" >}}  
&nbsp;

#### SHOW NOTES

A bit of a meandering rant on some of the reasons that I think make Linux superior to Windows.   
