---
title: "Chat With Patrons (March 29, 2020)"
image: images/thumbs/0582.jpg
date: 2020-03-29T12:22:40+06:00
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(March+29%2C+2020)-VYfSKl7YhI4.mp4" >}}
&nbsp;

#### SHOW NOTES

This Video Chat will be for my Patrons!  Patrons can join the video call via Zoom which is available on Linux, Mac and Windows.  For those wishing to join the chat but are not a Patron, consider joining [my Patreon](https://www.patreon.com/distrotube).  I will post a link to the Zoom chat on my Patreon page a few minutes prior to the stream.  Hope to see you guys there!