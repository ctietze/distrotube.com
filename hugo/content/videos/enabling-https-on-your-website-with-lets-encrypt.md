---
title: "Enabling HTTPS On Your Website With Let's Encrypt"
image: images/thumbs/0491.jpg
date: 2020-01-04T12:22:40+06:00
author: Derek Taylor
tags: ["Web Development", ""]
---

#### VIDEO

{{< amazon src="Enabling+HTTPS+On+Your+Website+With+Lets+Encrypt.mp4" >}}
&nbsp;

#### SHOW NOTES

Let’s Encrypt is a free, automated, and open certificate authority.  It allows you to create the digital certificates needed to enable HTTPS (SSl/TLS) for your websites.  And it's free!

+ ► https://letsencrypt.org/ - Let's Encrypt
+ ► https://certbot.eff.org/
