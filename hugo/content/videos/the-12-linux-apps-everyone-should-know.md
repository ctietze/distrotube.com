---
title: "The 12 Linux Apps Everyone Should Know About" 
image: images/thumbs/0759.jpg
date: 2020-11-21T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "TUI Apps"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-12-linux-apps-everyone-should-know/d918811013ee432e9bca71eb8e78f205017823f1?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

This is a top 12 list of Linux apps that I think everyone should know about, and probably should be using.  Most of the programs I list are cross-platform, so you could use them on Windows/Mac as well.  All of the programs I list will be free and open source software.

REFERENCED:
+  https://www.mozilla.org/
+  https://brave.com/
+  https://www.videolan.org/vlc/index.html
+  https://www.libreoffice.org/
+  https://jitsi.org/
+  https://www.gimp.org/
+  https://inkscape.org/
+  https://github.com/oguzhaninan/Stacer
+  https://htop.dev/
+  https://notepadqq.com/s/
+  https://app-outlet.github.io/
+  https://deadbeef.sourceforge.io/
+  https://www.virtualbox.org/
+  https://www.vim.org/
+  https://www.gnu.org/software/emacs/

