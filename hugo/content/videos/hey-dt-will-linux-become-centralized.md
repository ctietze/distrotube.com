---
title: "Hey DT, Will Linux Become Centralized? (Plus Other Questions Answered)"
image: images/thumbs/0616.jpg
date: 2020-05-07T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Will+Linux+Become+Centralized+And+Other+Questions+From+Viewers.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  I discuss my thoughts on Linux and the free software movement becoming "centralized", questions I've gotten regarding my terminals and window managers, as well as explaining why I've resorted to giving out RTFM's like it's Christmas!