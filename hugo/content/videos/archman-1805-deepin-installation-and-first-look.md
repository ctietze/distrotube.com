---
title: "Archman 18.05 Deepin Installation and First Look"
image: images/thumbs/0214.jpg
date: Wed, 23 May 2018 23:34:43 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Archman"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archman-18-05-deepin-installation-and/c4ef0690d11da8ab645cfb47c629c41a6e605e6f?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm taking a look at Archman 18.05 'Deepin'. This is yet another Arch-based Linux distribution that uses the Calamares installer, pamac, octopi and attempts to make Arch into a new user-friendly distro. http://archman.org/
