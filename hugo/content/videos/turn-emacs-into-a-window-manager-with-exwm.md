---
title: "Turn Emacs Into A Window Manager With EXWM"
image: images/thumbs/0655.jpg
date: 2020-06-27T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", "tiling window manager"]
---

#### VIDEO

{{< amazon src="Turn+Emacs+Into+A+Window+Manager+With+EXWM.mp4" >}}
&nbsp;

#### SHOW NOTES

Emacs isn't a text editor.  It's a desktop environment!  I used to think that was a joke.  But they weren't joking.  Install an Emacs plugin called EXWM (Emacs X Window Manager) and add a few lines to your emacs config.  Voila!  Emacs is your window manager.

REFERENCED:
+ https://www.gnu.org/software/emacs/ - GNU Emacs
+ https://github.com/hlissner/doom-emacs - Doom Emacs
+ https://github.com/ch11ng/exwm - EXWM