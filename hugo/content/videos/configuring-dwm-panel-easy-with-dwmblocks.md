---
title: "Configuring Dwm's Panel Is Easy With Dwmblocks" 
image: images/thumbs/0763.jpg
date: 2020-11-27T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "dwm"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/configuring-dwm-s-panel-is-easy-with/19d2f3781fd35ca41cb515261c21fa9f3a9d66a5?r=5CrPKPUnJzzsypDPZpQ6Fa8YzmPkL87R" allowfullscreen></iframe>

#### SHOW NOTES

Dwm has a builtin panel that can be a bit tough to configure.  Getting it to display the information that you want is not as simple as it should be.  Thankfully, there is a program called dwmblocks that makes this a lot easier!

