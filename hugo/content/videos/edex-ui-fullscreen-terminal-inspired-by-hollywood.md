---
title: "eDEX-UI - Fullscreen Terminal Inspired By Hollywood"
image: images/thumbs/0325.jpg
date: Mon, 31 Dec 2018 19:44:48 +0000
author: Derek Taylor
tags: ["terminal", ""]
---

#### VIDEO

{{< amazon src="eDEX+UI+Fullscreen+Terminal+Inspired+By+Hollywood.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at an Electron-based terminal emulator 
that was heavily inspired by science fiction movies.  That terminal 
emulator is eDEX-UI.  It is attractive, has touchscreen capabilities and
 is bloated as hell!  

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2FGitSquared%2Fedex-ui&amp;redir_token=RTDUAeMNgwu56XnNKggyQgmqOn58MTU1MzU0MzA4MkAxNTUzNDU2Njgy&amp;event=video_description&amp;v=G_4Cv-AP7xU" rel="noreferrer noopener" target="_blank">https://github.com/GitSquared/edex-ui
