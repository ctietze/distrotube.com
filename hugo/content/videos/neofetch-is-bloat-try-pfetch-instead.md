---
title: "Neofetch is BLOAT! Try pfetch instead."
image: images/thumbs/0444.jpg
date: Thu, 26 Sep 2019 23:49:00 +0000
author: Derek Taylor
tags: ["terminal", "command line", "bloat"]
---

#### VIDEO

{{< amazon src="Neofetch+is+bloat+Try+pfetch+instead.mp4" >}}
&nbsp;

#### SHOW NOTES

I quit using neofetch a few months back because I thought it was slow and bloated. I found a more minimal alternative--pfetch!


&nbsp;
#### REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=zput0Ir8XPM&amp;event=video_description&amp;q=https%3A%2F%2Fgithub.com%2Fdylanaraps%2Fneofetch&amp;redir_token=UfDKNKmNvOwvwXZnlr04-S0IDnZ8MTU3NzQ5MDU3MEAxNTc3NDA0MTcw" target="_blank" rel="nofollow noopener noreferrer">https://github.com/dylanaraps/neofetch
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=zput0Ir8XPM&amp;event=video_description&amp;q=https%3A%2F%2Fgithub.com%2Fdylanaraps%2Fpfetch&amp;redir_token=UfDKNKmNvOwvwXZnlr04-S0IDnZ8MTU3NzQ5MDU3MEAxNTc3NDA0MTcw" target="_blank" rel="nofollow noopener noreferrer">https://github.com/dylanaraps/pfetch
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=zput0Ir8XPM&amp;event=video_description&amp;q=https%3A%2F%2Fgitlab.com%2Fjschx%2Fufetch&amp;redir_token=UfDKNKmNvOwvwXZnlr04-S0IDnZ8MTU3NzQ5MDU3MEAxNTc3NDA0MTcw" target="_blank" rel="nofollow noopener noreferrer">https://gitlab.com/jschx/ufetch
