---
title: "A First Look At ArcoLinux With Dwm" 
image: images/thumbs/0757.jpg
date: 2020-11-18T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "ArcoLinux", "dwm"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-first-look-at-arcolinux-with-dwm/9f1d8b3f0890fb17013e7c1341f1b9f79856948c?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

In the last ArcoLinux recently released a new version (20.11.9). This release is the first one to offer a dwm edition of ArcoLinuxB. Since I love both Arco and dwm, I had to check this out.

REFERENCED:
+ https://arcolinux.info/arcolinux-d-b-20-11/

