---
title: "Chat With Patrons (October 21, 2018)"
image: images/thumbs/0489.jpg
date: Sun, 21 Oct 2018 19:02:44 +0000
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(October+21%2C+2018)-Oc_bQ9eSOwk.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be a special event for my Patreon supporters. This will be a Zoom video chat just for my patrons. The link to the video chat call will be posted on my Patreon page a few minutes prior to the stream.
