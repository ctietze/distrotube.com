---
title: "The Basics of Emacs as a Text Editor"
image: images/thumbs/0473.jpg
date: Tue, 03 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="The+Basics+of+Emacs+as+a+Text+Editor.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to quickly go through some of the basics of using Emacs, the text editor!  

#### REFERENCED:
+ ► https://www.gnu.org/software/emacs/
