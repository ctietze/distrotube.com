---
title: "Affordable XLR Mic For Streamers"
image: images/thumbs/0474.jpg
date: Wed, 04 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Audio", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/affordable-xlr-mic-for-streamers/44bee61b54a2c37e75281cba88646817e440f477?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I recently purchased a Blue Ember, an affordable XLR microphone that is marketed as a "streaming" mic.  Just got this thing delivered and will share the very first sounds of this mic with you guys.

#### REFERENCED:
+ Blue Ember - https://amzn.to/2DLJRZh
+ Blue Baby Bottle SL - https://amzn.to/2RkZRcT
+ Scarlett 2i2 Interface - https://amzn.to/2YjLB5A
