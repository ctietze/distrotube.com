---
title: "Looking At Six Flavors of Ubuntu 20.10 Groovy Gorilla" 
image: images/thumbs/0739.jpg
date: 2020-10-22T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/looking-at-six-flavors-of-ubuntu-20-10/9f9a9c8ea71250eaa40cb04b96a0ab06d3a69ae4?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video, I take a quick look at the recently released (or soon-to-be-released) versions of Ubuntu 20.10 codenamed "Groovy Gorilla."  I will briefly look at the flagship Ubuntu distribution as well as: Kubuntu, Xubuntu, Lubuntu, Ubuntu Budgie and Ubuntu MATE. 

REFERENCED:
+  https://releases.ubuntu.com/20.10/
+  https://kubuntu.org/
+  https://xubuntu.org/
+  https://lubuntu.me/
+  https://ubuntubudgie.org/
+  https://ubuntu-mate.org/