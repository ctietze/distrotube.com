---
title: "Redcore Linux 1812 LXQt - Installation and First Look"
image: images/thumbs/0349.jpg
date: Sat, 09 Feb 2019 22:51:39 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Redcore Linux"]
---

#### VIDEO

{{< amazon src="Redcore+Linux+1812+LXQt+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to install the newly released Redcore Linux  1812 LXQt edition and take a quick look.  Redcore is a Gentoo-based  distro out of Romania. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fredcorelinux.org%2F&amp;v=UvRxPofNeSw&amp;event=video_description&amp;redir_token=XZ4tcwzR2ObQmrpfXP20B5KKW7N8MTU1MzY0MDcyNEAxNTUzNTU0MzI0" target="_blank">https://redcorelinux.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fredcorelinux.org%2Fnews%2Fredcore-linux-1812-codename-luna-stable&amp;v=UvRxPofNeSw&amp;event=video_description&amp;redir_token=XZ4tcwzR2ObQmrpfXP20B5KKW7N8MTU1MzY0MDcyNEAxNTUzNTU0MzI0" target="_blank">https://redcorelinux.org/news/redcore...
