---
title: "Stacer - System Monitor, Optimizer and Cleaner"
image: images/thumbs/0049.jpg
date: Tue, 05 Dec 2017 14:40:01 +0000
author: Derek Taylor
tags: ["TUI Apps", "stacer"]
---

#### VIDEO

{{< amazon src="Stacer+-+System+Monitor%2C+Optimizer+and+Cleaner.mp4" >}}  
&nbsp;

#### SHOW NOTES

Stacer is an open source Electron application that combines the power of a system cleaner, system monitor, startup manager, and a package remover all in one easy-to-use program. <a href="https://github.com/oguzhaninan/Stacer">https://github.com/oguzhaninan/Stacer</a>
