---
title: "Switching to i3 - Day Four"
image: images/thumbs/0328.jpg
date: Fri, 04 Jan 2019 19:54:58 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm"]
---

#### VIDEO

{{< amazon src="Switching+to+i3+Day+Four.mp4" >}}
&nbsp;

#### SHOW NOTES

I asked you guys to help decide what window manager I should use in 2019. This is Day Four of me living in i3.
