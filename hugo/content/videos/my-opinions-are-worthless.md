---
title: "My Opinions Are Worthless"
image: images/thumbs/0480.jpg
date: Sat, 21 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="My+Opinions+Are+Worthless.mp4" >}}
&nbsp;

#### SHOW NOTES

I constantly get asked questions like "What is your favorite distro?" or "What is the best tiling window manager?"  Why are these people asking these questions?  In their quest to find the right tools for them, does it really matter what the right tools for me personally are?
