---
title: "VSIDO Installation and First Look"
image: images/thumbs/0226.jpg
date: Thu, 07 Jun 2018 23:56:09 +0000
author: Derek Taylor
tags: ["Distro Reviews", "VSIDO"]
---

#### VIDEO

{{< amazon src="VSIDO+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I'm taking a quick first look at VSIDO, a Linux distro built upon Debian Sid (unstable) and using the Fluxbox window manager. It boots up using 125MB of memory. Now that's light. http://vsido.org/index.php
