---
title: "A First Look At SpaceVim" 
image: images/thumbs/0731.jpg
date: 2020-10-13T12:23:40+06:00
author: Derek Taylor
tags: ["Vim", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-first-look-at-spacevim/4788f13a8da34a1e150659c874d54530a0478883?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

SpaceVim is a distribution of Vim inspired by Spacemacs (a distribution of Emacs).  It comes with a bunch of plugins installed and configured out of the box, which makes SpaceVim easier to get into for the the new Vim user.  After spending so much time recently in Doom Emacs, what will be my initial impressions of SpaceVim?

#### REFERENCED:
+ https://spacevim.org/