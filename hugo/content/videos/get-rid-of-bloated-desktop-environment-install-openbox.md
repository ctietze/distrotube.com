---
title: "Get Rid Of That Bloated Desktop Environment And Install Openbox" 
image: images/thumbs/0720.jpg
date: 2020-09-22T12:23:40+06:00
author: Derek Taylor
tags: ["Openbox", ""]
---

#### VIDEO

{{< amazon src="Get+Rid+Of+That+Bloated+Desktop+Environment+And+Install+Openbox.mp4" >}}
&nbsp;

#### SHOW NOTES

So you're tired of that heavy, bloated desktop environment.  And you're ready to go "window manager only."  But tiling window managers seem too intimidating or too complicated.  Well, one of my favorite window managers is actually a floating window manager called Openbox.  In this lengthy video, I will install Openbox on Linux Mint and show the basics of getting started.

REFERENCED:
+ http://openbox.org/wiki/Main_Page