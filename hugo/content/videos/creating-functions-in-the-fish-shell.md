---
title: "Creating Functions In The Fish Shell" 
image: images/thumbs/0747.jpg
date: 2020-11-03T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/creating-functions-in-the-fish-shell/0a6e40c72eba5f760c554ae300a2ab4f219ac9f8?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video, I will briefly go over some of the settings that I have in my Fish configuration file (config.fish).  I will also discuss some cool functions that you can add to your config.  And everything I do here is done without using oh-my-fish.