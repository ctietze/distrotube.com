---
title: "Live Feb 20, 2018 - Updating some VMs and discussing Linux-type stuff"
image: images/thumbs/0118.jpg
date: Tue, 20 Feb 2018 01:58:58 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Live+Feb+20%2C+2018+-+Updating+some+VMs+and+discussing+Linux-type+stuff.mp4" >}}  
&nbsp;

#### SHOW NOTES

Just going to login to my virtual machines and update them. Got alot of VMs so this may take awhile. May get into some Linux news and read some viewer comments as well. 
