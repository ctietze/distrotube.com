---
title: "Unfettered Freedom, Ep. 3 - Facebook, Zoom, AppImage, Kdenlive, New Linux Users"
image: images/thumbs/0697.jpg
date: 2020-08-19T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+3+-+Facebook%2C+Zoom%2C+AppImage%2C+Kdenlive%2C+New+Users.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 1:38 - Facebook becomes platinum member of Linux Foundation.
+ 6:02 - Trial by proprietary software.
+ 13:21 - Why one software maker chooses AppImage.
+ 17:21 - Kdenlive 20.08 is a huge release!
+ 22:17 - Are we presenting Linux in a friendly way to new users?
+ 28:15 - Thanks to the Patrons!  Check out DT on Patreon.