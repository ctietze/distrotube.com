---
title: "The Linux Community Is Not Your 'Safe Space'"
image: images/thumbs/0407.jpg
date: Fri, 31 May 2019 03:29:56 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Linux+Community+Is+Not+Your+Safe+Space.mp4" >}}
&nbsp;

#### SHOW NOTES

Is having differing opinions no longer acceptable in today's society? Is it no longer acceptable in the Linux community? Do opinions that you do not agree with "trigger" you? Do you need to go to a "safe space"?


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=JjxUPd1SD0k&amp;event=video_description&amp;q=https%3A%2F%2Fwww.techrepublic.com%2Farticle%2Fscientific-linux-and-antergos-are-shutting-down-its-time-for-linux-mint-to-go%2F&amp;redir_token=s2qkIxH87Hf_ysEArV4ctOA6Sdx8MTU2NjUzMTAwMUAxNTY2NDQ0NjAx" target="_blank" rel="nofollow noopener noreferrer">https://www.techrepublic.com/article/...
