---
title: "MX Linux 18 'Continuum' - Taking It For A Quick Spin"
image: images/thumbs/0321.jpg
date: Sat, 22 Dec 2018 19:38:04 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MX Linux"]
---

#### VIDEO

{{< amazon src="MX+Linux+18+Continuum+Taking+It+For+A+Quick.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at the recently released MX Linux 18, codenamed
 "Continuum".  MX is a Debian-based Linux distribution that sports a 
nice, customized XFCE desktop environment.  

<a href="https://www.youtube.com/redirect?event=video_description&amp;v=nsd3cXRJ_bs&amp;redir_token=FZYrDprpJe4yrY0s9-4fClGlWzh8MTU1MzU0MjY5NkAxNTUzNDU2Mjk2&amp;q=https%3A%2F%2Fmxlinux.org%2F" rel="noreferrer noopener" target="_blank">https://mxlinux.org/
