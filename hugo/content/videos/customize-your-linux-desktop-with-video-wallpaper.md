---
title: "Customize Your Linux Desktop With A Video Wallpaper"
image: images/thumbs/0671.jpg
date: 2020-07-18T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Customize+Your+Linux+Desktop+With+A+Video+Wallpaper.mp4" >}}
&nbsp;

#### SHOW NOTES

Wallset is a wallpaper manager that makes it possible to put videos as wallpaper.  That's right!  No more boring, static wallpapers that are just photos of your new kitten.  Why not take a video of that kitten and set it as your wallpaper?

REFERENCED:
+ https://github.com/terroo/wallset