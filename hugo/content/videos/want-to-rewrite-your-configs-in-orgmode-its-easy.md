---
title: "Want To Rewrite Your Configs In Org-Mode? It's Easy!"
image: images/thumbs/0670.jpg
date: 2020-07-17T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs"]
---

#### VIDEO

{{< amazon src="Want+To+Rewrite+Your+Configs+In+Org-Mode+Its+Easy.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the killer features of Emacs is Org-Mode, which is a kind of markdown that makes creating outlines, tables, source code blocks, and tables of content really easy.  Often, when people discover Org-Mode, they start wanting to rewrite all of their configs as literate configs in Org. 


ERRATA:
I mentioned in the video that this would work on any config file written in an actual programming language.  That 's not actually the case. Tangle will work on anything, so it doesn't have to be a programming language, since we are not really evaluating the code with this.  We are just writing what's in the SRC blocks to the file we specify.


REFERENCED:
+ https://www.gnu.org/software/emacs/ - GNU Emacs
+ https://github.com/hlissner/doom-emacs - Doom Emacs
+ https://orgmode.org/ - Org Mode Documentation