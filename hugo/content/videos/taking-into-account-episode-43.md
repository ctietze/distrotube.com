---
title: "Taking Into Account, Ep. 43 - Huawei, South Korea, Firefox, GitHub Sponsors, Manjaro, Antergos"
image: images/thumbs/0401.jpg
date: Thu, 23 May 2019 14:47:27 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+43.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
0:48 Chinese telecom Huawei receives the ban hammer. Here's a timeline on how and why this happened.
+ 11:11 Windows 10 is not an option. South Korea is switching to Linux ahead of the Windows 7 shutdown.
+ 15:28 Firefox 67 is here and it's faster than ever. Chrome users, maybe it's time to look at Firefox again?
+ 19:20 Announcing GitHub Sponsors, a new way to contribute to your favorite open source projects.
+ 23:37 Nearly 1.1 million downloads of Manjaro since the first of the year. Continues to rise in popularity.
+ 28:13 I read viewer comment from Mastodon regarding the death of Antergos.


&nbsp;
#### REFERENCED:
+ https://www.cnet.com/news/huawei-ban-full-timeline-on-how-and-why-its-phones-were-banned-security/
+ https://www.theinquirer.net/inquirer/news/3076048/south-korea-is-switching-to-linux-ahead-of-the-windows-7-shutdown
+ https://blog.mozilla.org/blog/2019/05/21/latest-firefox-release-is-faster-than-ever/
+ https://github.blog/2019-05-23-announcing-github-sponsors-a-new-way-to-contribute-to-open-source/
+ https://twitter.com/ManjaroLinux/status/1131296923640573953
+ https://www.youtube.com/watch?v=Co6FePZoNgE
