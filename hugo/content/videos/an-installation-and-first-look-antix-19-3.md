---
title: "An Installation And First Look At AntiX 19.3" 
image: images/thumbs/0735.jpg
date: 2020-10-17T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/an-installation-and-first-look-at-antix/7b61e2e20e42ee7eec05047568fe273c43f3ef6e?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

This is an installation and first look at the recently released AntiX 19.3.  AntiX is a Debian-based Linux distribution that is light, fast and gorgeous.  It is systemd-free and it has both 32-bit and 64-bit ISOs.  If you have really old machines that cannot run other Linux distros, give AntiX a try.  

REFERENCED:
+ https://antixlinux.com/