---
title: "Why Desktop Environment Users Don't Understand Tiling Window Managers"
image: images/thumbs/0664.jpg
date: 2020-07-09T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", ""]
---

#### VIDEO

{{< amazon src="Why+Desktop+Environment+Users+Don't+Understand+Tiling+Window+Managers.mp4" >}}
&nbsp;

#### SHOW NOTES

I keep getting this question asked: "Why do all tiling window managers look the same and function the same?"  It is a fair question, especially if all you know is the big, traditional desktop environments.