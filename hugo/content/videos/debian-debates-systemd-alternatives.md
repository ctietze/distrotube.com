---
title: "Debian Debates Systemd Alternatives"
image: images/thumbs/0481.jpg
date: Mon, 23 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["systemd", "Debian"]
---

#### VIDEO

{{< amazon src="Debian+Debates+Systemd+Alternatives.mp4" >}}
&nbsp;

#### SHOW NOTES

Debian is debating their init system choices...again.  Should Debian remain focused on systemd or should there be a path to one or more alternative init systems?  Soon there will be a vote.  On the ballot, there will be seven options.  
	
#### REFERENCED:
+ ► https://lwn.net/Articles/806332/
