---
title: "The Best Tiling Window Manager" 
image: images/thumbs/0754.jpg
date: 2020-11-12T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "xmonad"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/the-best-tiling-window-manager/541e957f7759942aafaaf42a13e7e19a7f58fc91?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

I am often asked, "What is your favorite window manager?" or "What is the best window manager?" Well, I thought I would give my opinion, even though I don't speak in this video. I just let the window manager do all the talking.

REFERENCED:
+ https://xmonad.org/

